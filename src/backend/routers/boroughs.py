from fastapi import APIRouter, Depends, HTTPException, status
from .. import schemas, models
from sqlalchemy.orm import Session
from sqlalchemy import asc, desc, or_
from ..db import get_db

router = APIRouter(
    prefix='/Boroughs'
)


@router.get('/', response_model=schemas.Boroughs)
def get_pantries(db: Session = Depends(get_db), limit: int = 24, skip: int = 0, search: str = '', sort: str = None, order: str = 'asc', borough: str = '', community_district_num: int = 1):
    query = db.query(models.Boroughs)
    if search:
        query = query.filter(or_(
            models.Boroughs.borough.ilike(f'%{search}%'),
            models.Boroughs.community_district_num.ilike(f'%{search}%'),  # Replace field1, field2, etc. with the actual fields you want to search
            models.Boroughs.adult_buildings.ilike(f'%{search}%'),
            models.Boroughs.family_buildings.ilike(f'%{search}%'),
            models.Boroughs.fwc_buildings.ilike(f'%{search}%'),
            models.Boroughs.individual_count.ilike(f'%{search}%'),
            models.Boroughs.cases_count.ilike(f'%{search}%')
            # Add more fields as needed
        ))
    if borough:
        query = query.filter(models.Boroughs.borough.ilike(f'%{borough}%'))
    if sort:
        if sort == 'name':
            query = query.order_by(asc(models.Boroughs.borough)) if order == 'asc' else query.order_by(desc(models.Boroughs.borough))
        elif sort == 'community_district_num':
            query = query.order_by(asc(models.Boroughs.community_district_num)) if order == 'asc' else query.order_by(desc(models.Boroughs.community_district_num))
        elif sort == 'adult_buildings':
            query = query.order_by(asc(models.Boroughs.adult_buildings)) if order == 'asc' else query.order_by(desc(models.Boroughs.adult_buildings))
        elif sort == 'family_buildings':
            query = query.order_by(asc(models.Boroughs.family_buildings)) if order == 'asc' else query.order_by(desc(models.Boroughs.family_buildings))
        elif sort == 'fwc_buildings':
            query = query.order_by(asc(models.Boroughs.fwc_buildings)) if order == 'asc' else query.order_by(desc(models.Boroughs.fwc_buildings))
        elif sort == 'individual_count':
            query = query.order_by(asc(models.Boroughs.individual_count)) if order == 'asc' else query.order_by(desc(models.Boroughs.individual_count))
        elif sort == 'cases_count':
            query = query.order_by(asc(models.Boroughs.cases_count)) if order == 'asc' else query.order_by(desc(models.Boroughs.cases_count))

    count = query.count()
    results = query.offset(skip).limit(limit).all()
    return {'results': results, 'count': count}

@router.get('/{id}', response_model=schemas.Borough)
def get_borough(id: int, db: Session = Depends(get_db)):
    borough = db.query(models.Boroughs).filter(models.Boroughs.id == id).first()
    if not borough:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Borough with id {id} does not exist')
    return borough

