from fastapi import APIRouter, Depends, HTTPException, status
from .. import schemas, models
from sqlalchemy.orm import Session
from sqlalchemy import asc, desc, or_
from ..db import get_db, SessionLocal
from fastapi.templating import Jinja2Templates
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix='/Shelters'
)
templates = Jinja2Templates(directory='backend/templates')

@router.get('/', response_model=schemas.Shelters)
def get_shelters(db: Session = Depends(get_db), limit: int = 24, skip: int = 0, search: str = '', sort: str = None, order: str = 'asc', borough: str = '', program_type: str = ''):
    try:
        query = db.query(models.Shelters) # failing?
        if search:
            query = query.filter(or_(
                models.Shelters.name.ilike(f'%{search}%'),
                models.Shelters.borough.ilike(f'%{search}%'),  # Replace field1, field2, etc. with the actual fields you want to search
                models.Shelters.address.ilike(f'%{search}%'),
                models.Shelters.phone.ilike(f'%{search}%'),
                models.Shelters.program_type.ilike(f'%{search}%')
                # Add more fields as needed
            ))
        if borough: 
            query = query.filter(models.Shelters.borough.ilike(f'%{borough}%'))
        if program_type:
            query = query.filter(models.Shelters.program_type.ilike(f'%{program_type}%'))
        if sort:
            if sort == 'name':
                query = query.order_by(asc(models.Shelters.name)) if order == 'asc' else query.order_by(desc(models.Shelters.name))
            elif sort == 'borough':
                query = query.order_by(asc(models.Shelters.borough)) if order == 'asc' else query.order_by(desc(models.Shelters.borough))
            elif sort == 'program_type':
                query = query.order_by(asc(models.Shelters.program_type)) if order == 'asc' else query.order_by(desc(models.Shelters.program_type))
            elif sort == 'address':
                query = query.order_by(asc(models.Shelters.address)) if order == 'asc' else query.order_by(desc(models.Shelters.address))
            elif sort == 'phone':
                query = query.order_by(asc(models.Shelters.phone)) if order == 'asc' else query.order_by(desc(models.Shelters.phone))

        count = query.count()
        results = query.offset(skip).limit(limit).all()
        return {'results': results, 'count': count}
    except Exception as e:
        logger.error(f"Error fetching shelters: {str(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal Server Error")

@router.get('/{id}', response_model=schemas.Shelter)
def get_shelter(id: str, db: Session = Depends(get_db)):
    shelter = db.query(models.Shelters).filter(models.Shelters.id == id).first()
    if not shelter:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Shelter with id {id} does not exist')
    return shelter

