from fastapi import APIRouter, Depends, HTTPException, status
from .. import schemas, models
from sqlalchemy.orm import Session
from sqlalchemy import asc, desc, or_
from ..db import get_db

router = APIRouter(
    prefix='/Pantries'
)


@router.get('/', response_model=schemas.Pantries)
def get_pantries(db: Session = Depends(get_db), limit: int = 24, skip: int = 0, search: str = '', sort: str = None, order: str = 'asc', borough: str = '', site_type: str = '', approval_license_type: str = ''):
    query = db.query(models.Pantries)
    if search:
        query = query.filter(or_(
            models.Pantries.name.ilike(f'%{search}%'),
            models.Pantries.borough.ilike(f'%{search}%'),  # Replace field1, field2, etc. with the actual fields you want to search
            models.Pantries.address.ilike(f'%{search}%'),
            models.Pantries.site_type.ilike(f'%{search}%'),
            models.Pantries.program_type.ilike(f'%{search}%'),
            models.Pantries.approval_license_type.ilike(f'%{search}%')
            # Add more fields as needed
        ))
    if borough:
        query = query.filter(models.Pantries.borough.ilike(f'%{borough}%'))
    if site_type:
        query = query.filter(models.Pantries.site_type.ilike(f'%{site_type}%'))
    if approval_license_type:
        query = query.filter(models.Pantries.approval_license_type.ilike(f'%{approval_license_type}%'))
    if sort:
        if sort == 'name':
            query = query.order_by(asc(models.Pantries.name)) if order == 'asc' else query.order_by(desc(models.Pantries.name))
        elif sort == 'borough':
            query = query.order_by(asc(models.Pantries.borough)) if order == 'asc' else query.order_by(desc(models.Pantries.borough))
        elif sort == 'site_type':
            query = query.order_by(asc(models.Pantries.site_type)) if order == 'asc' else query.order_by(desc(models.Pantries.site_type))
        elif sort == 'approval_license_type':
            query = query.order_by(asc(models.Pantries.approval_license_type)) if order == 'asc' else query.order_by(desc(models.Pantries.approval_license_type))
        elif sort == 'address':
            query = query.order_by(asc(models.Pantries.address)) if order == 'asc' else query.order_by(desc(models.Pantries.address))
        
    count = query.count()
    results = query.offset(skip).limit(limit).all()
    return {'results': results, 'count': count}

@router.get('/{id}', response_model=schemas.Pantry)
def get_pantry(id: int, db: Session = Depends(get_db)):
    pantry = db.query(models.Pantries).filter(models.Pantries.id == id).first()
    if not pantry:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Pantry with id {id} does not exist')
    return pantry

