from typing import Optional, List
from pydantic import BaseModel, root_validator
from datetime import datetime

class BaseShelter(BaseModel):
    id: int
    name: str


class BasePantry(BaseModel):
    id: int
    name: str


class BaseBorough(BaseModel):
    id: int
    borough: str


class Shelter(BaseShelter):
    borough: Optional[str]
    address: Optional[str]
    phone: Optional[str]
    program_type: Optional[str]
    image_url: Optional[str]

    class Config:
        from_attributes = True


class Shelters(BaseModel):
    results: list[Shelter]
    count: int


class Pantry(BasePantry):
    borough: Optional[str]
    address: Optional[str]
    site_type: Optional[str]
    program_type: Optional[str]
    approval_license_type: Optional[str]
    image_url: Optional[str]
    
    class Config:
        from_attributes = True


class Pantries(BaseModel):
    results: list[Pantry]
    count: int


class Borough(BaseBorough):
    community_district_num: Optional[int]
    adult_buildings: Optional[int]
    family_buildings: Optional[int]
    fwc_buildings: Optional[int]
    individual_count: Optional[int]
    cases_count: Optional[int]
    image_url: Optional[str]

    class Config:
        from_attributes = True


class Boroughs(BaseModel):
    results: list[Borough]
    count: int