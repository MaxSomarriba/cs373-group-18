from pydantic_settings import BaseSettings # will grab .env file and populate env variables
from dotenv import load_dotenv

load_dotenv()
class Settings(BaseSettings):
    nyc_opendata_key: str
    google_api_key: str
    google_cx: str
    google_maps_api_key: str
    db_host: str
    db_port: int
    db_user: str
    db_password: str
    db_database: str

    class Config:
        env_file = 'backend/.env'

settings = Settings()