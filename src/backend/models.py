from .db import Base
from enum import Enum as EnumBase
from sqlalchemy import Column, Enum, Integer, String # model col types
from sqlalchemy.orm import relationship

class Shelters(Base):
    __tablename__ = 'shelters'
    # sqlalchemy automatically serializes the id
    id = Column(Integer, primary_key=True)

    name = Column(String(255), nullable=False)
    borough = Column(String(255), nullable=True)
    address = Column(String(255), nullable=True)
    phone = Column(String(255), nullable=True)
    program_type = Column(String(255), nullable=False)
    image_url = Column(String(255), nullable=True)

class Pantries(Base):
    __tablename__ = 'pantries'
    id = Column(Integer, primary_key=True)

    name = Column(String(255), nullable=False)
    borough = Column(String(255), nullable=True)
    address = Column(String(255), nullable=True)
    site_type = Column(String(255), nullable=True)
    program_type = Column(String(255), nullable=True)
    approval_license_type = Column(String(255), nullable=True)
    image_url = Column(String(255), nullable=True)


class Boroughs(Base): # boroughs + community districts
    __tablename__ = 'boroughs'
    id = Column(Integer, primary_key=True)

    borough = Column(String(255), nullable=False)
    community_district_num = Column(Integer, nullable=False)
    # community_district_desc = Column(Enum(CommunityDistrictEnum), nullable=False)
    adult_buildings = Column(Integer, nullable=True)
    family_buildings = Column(Integer, nullable=True)
    fwc_buildings = Column(Integer, nullable=True)
    individual_count = Column(Integer, nullable=True)
    cases_count = Column(Integer, nullable=True)
    image_url = Column(String(255), nullable=True)


