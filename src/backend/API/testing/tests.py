import unittest
from unittest.mock import patch, Mock
from fastapi import HTTPException
from fastapi.testclient import TestClient
from backend.main import app 

class TestAPIEndpoints(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    @patch('backend.db.get_db')
    def test_get_shelters(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.count.return_value = 5
        mock_query.limit.return_value.all.return_value = ["shelter1", "shelter2"]
        mock_get_db.return_value = mock_session

        response = self.client.get("/Shelters/?limit=1000")
        self.assertIsNotNone(response.json())
        self.assertEqual(len(response.json().get('results')), 125)

    @patch('backend.db.get_db')
    def test_get_shelter(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = "shelter1"

        mock_get_db.return_value = mock_session

        response = self.client.get("/Shelters/1")
        self.assertIsNotNone(response.json())

    @patch('backend.db.get_db')
    def test_get_shelter_not_found(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = None

        mock_get_db.return_value = mock_session
        response = self.client.get("/Shelters/999")
        self.assertEqual(response.status_code, 404)

    @patch('backend.db.get_db')
    def test_get_pantries(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.count.return_value = 5
        mock_query.limit.return_value.all.return_value = ["pantry1", "pantry2"]

        mock_get_db.return_value = mock_session

        response = self.client.get("/Pantries/?limit=1000")
        self.assertIsNotNone(response.json())
        self.assertEqual(len(response.json().get('results')), 250)

    @patch('backend.db.get_db')
    def test_get_pantry(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = "pantry1"

        mock_get_db.return_value = mock_session

        response = self.client.get("/Pantries/1")
        self.assertIsNotNone(response.json())

    @patch('backend.db.get_db')
    def test_get_pantry_not_found(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = None

        mock_get_db.return_value = mock_session
        response = self.client.get("/Pantries/999")
        self.assertEqual(response.status_code, 404)

    @patch('backend.db.get_db')
    def test_get_boroughs(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.count.return_value = 5
        mock_query.limit.return_value.all.return_value = ["borough1", "borough2", "borough3"]

        mock_get_db.return_value = mock_session

        response = self.client.get("/Boroughs/?limit=1000")
        self.assertIsNotNone(response.json())
        self.assertEqual(len(response.json().get('results')), 59)

    @patch('backend.db.get_db')
    def test_get_borough(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = "borough1"

        mock_get_db.return_value = mock_session

        response = self.client.get("/Boroughs/1")
        self.assertIsNotNone(response.json())

    @patch('backend.db.get_db')
    def test_get_borough_not_found(self, mock_get_db):
        mock_session = Mock()
        mock_query = mock_session.query.return_value
        mock_query.filter.return_value.first.return_value = None

        mock_get_db.return_value = mock_session
        response = self.client.get("/Boroughs/999")
        self.assertEqual(response.status_code, 404)

if __name__ == "__main__":
    unittest.main()
