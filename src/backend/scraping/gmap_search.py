import requests
from ..config import settings
from .. import models
from ..db import SessionLocal

def get_location_info(location_name):
    params = {
        'address': location_name,
        'key': settings.google_maps_api_key,
    }

    response = requests.get("https://maps.googleapis.com/maps/api/geocode/json", params=params)
    results = response.json()

    if results['status'] == 'OK' and results.get('results'):
        first_result = results['results'][0]
        place_id = first_result.get('place_id')
        address = first_result.get('formatted_address', 'N/A')
        borough = get_borough_from_result(first_result)
        phone = get_phone_from_place_id(place_id)
        return address, borough, phone

    return None, None, None

def get_borough_from_result(result):
    for component in result.get('address_components', []):
        if 'sublocality' in component['types']:
            return component['long_name']
    return 'N/A'

def get_phone_from_place_id(place_id):
    if place_id:
        params = {
            'place_id': place_id,
            'key': settings.google_maps_api_key,
        }
        details_response = requests.get("https://maps.googleapis.com/maps/api/place/details/json", params=params)
        details_results = details_response.json()

        if details_results['status'] == 'OK':
            formatted_phone_number = details_results['result'].get('formatted_phone_number')
            return formatted_phone_number

    return 'N/A'