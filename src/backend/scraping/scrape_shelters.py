import requests
import json
import html
from .. import models
from ..config import settings
from ..db import SessionLocal
from .gmap_search import get_location_info

def scrape_homebase():
    url = 'https://data.cityofnewyork.us/resource/ntcm-2w4k.json'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    for record in homebases:
        entry = models.Shelters(
            name=record['homebase_office'],
            borough=record['borough'], #enum name, not value
            address=record['address'],
            phone=record['phone_number'],
            program_type='HOMEBASE'
        )
        entries.append(entry)
        print(f'Scraped {entry.name}')

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()
    
def scrape_family():
    url = 'https://data.cityofnewyork.us/resource/y7z5-rhh5.json'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    family = response.json()

    entries = []
    for record in family:
        if record['year_quarter'] == '2013 Q1': # don't want same records over multiple years
            x = record['provider_agency']
            y = record['facility_name']
            address, borough, phone = get_location_info(f'nyc {x} {y}')
            entry = models.Shelters(
                name=record['facility_name'],
                borough=borough,
                address=address,
                phone=phone,
                program_type='FAMILY'
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()

def scrape_adult():
    url = 'https://data.cityofnewyork.us/resource/jhn3-4vdj.json'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    adult = response.json()

    entries = []
    for record in adult:
        if record['year_quarter'] == '2011Q4': # don't want same records over multiple years\
            x = record['provider_agency']
            y = record['facility_name']
            address, borough, phone = get_location_info(f'nyc {x} {y}')
            entry = models.Shelters(
                name=record['facility_name'],
                borough=borough,
                address=address,
                phone=phone,
                program_type='ADULT'
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()

def scrape_youth():
    url = 'https://data.ny.gov/resource/q88j-j2mi.json'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    adult = response.json()

    entries = []
    for record in adult:
        x = record['street_address']
        _, borough, _ = get_location_info(f'nyc {x}')
        entry = models.Shelters(
            name=record['agency'],
            borough=borough,
            address=record['street_address'],
            phone=record['phone'],
            program_type='YOUTH'
        )
        entries.append(entry)
        print(f'Scraped {entry.name}')

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()

def create_shelters_model():
    scrape_homebase()
    scrape_family()
    scrape_adult()
    scrape_youth()

    db = SessionLocal()
    # Delete rows where the borough is 'N/A' or NULL
    db.query(models.Shelters).filter((models.Shelters.borough == 'N/A') | (models.Shelters.borough.is_(None))).delete()
    db.commit()
    db.close()
