import React, { useState, useEffect } from "react";
import axios from "axios";

const GoogleMapNameToAddress = ({ locationName }) => {
  const [address, setAddress] = useState("");
  const [embedMapLink, setEmbedMapLink] = useState("");

  useEffect(() => {
    const fetchAddress = async () => {
      try {
        const apiKey = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;
        const response = await axios.get(
          `https://maps.googleapis.com/maps/api/geocode/json?address=${locationName}&key=${apiKey}`
        );

        const { results } = response.data;
        if (results.length > 0) {
          const formattedAddress = results[0].formatted_address;
          const { lat, lng } = results[0].geometry.location;

          setAddress(formattedAddress);
          setEmbedMapLink(
            `https://www.google.com/maps/embed/v1/place?q=${lat},${lng}&key=${apiKey}`
          );
        } else {
          setAddress("Location not found");
          setEmbedMapLink("");
        }
      } catch (error) {
        console.error("Error fetching data from Google Maps API:", error);
      }
    };

    fetchAddress();
  }, [locationName]);

  return (
    <div>
      <h2>Location Information</h2>
      <p>Location Name: {locationName}</p>
      <p>Address: {address}</p>

      {embedMapLink && (
        <iframe
          title="Google Map Embed"
          width="600"
          height="450"
          style={{ border: 0 }}
          src={embedMapLink}
          allowFullScreen
        ></iframe>
      )}
    </div>
  );
};

export default GoogleMapNameToAddress;
