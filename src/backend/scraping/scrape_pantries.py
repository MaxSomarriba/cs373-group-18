import requests
from .. import models
from ..config import settings
from ..db import SessionLocal
from .gmap_search import get_location_info

def scrape_bronx():
    url = 'https://health.data.ny.gov/resource/dmn7-mpa8.json?$query=SELECT%0A%20%20%60county%60%2C%0A%20%20%60site_name%60%2C%0A%20%20%60address_omitted%60%2C%0A%20%20%60street1%60%2C%0A%20%20%60street2%60%2C%0A%20%20%60city%60%2C%0A%20%20%60state%60%2C%0A%20%20%60zipcode%60%2C%0A%20%20%60program_type%60%2C%0A%20%20%60approval_license_type%60%2C%0A%20%20%60site_type%60%2C%0A%20%20%60breastfeeding_friendly_certified%60%2C%0A%20%20%60eat_well_play_hard_ccs_participant%60%2C%0A%20%20%60last_ewphccs_participation%60%2C%0A%20%20%60eat_well_play_hard_dch_participant%60%2C%0A%20%20%60last_ewphdch_participation%60%2C%0A%20%20%60location%60%0AWHERE%20caseless_one_of(%60county%60%2C%20%22BRONX%22)%20AND%20(%60street1%60%20IS%20NOT%20NULL)'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    numRecord = 1
    for record in homebases:
        if numRecord <= 50:
            entry = models.Pantries(
                name=record['site_name'],
                borough=record['county'], 
                address=record['street1'],
                site_type=record['site_type'],
                program_type=record['program_type'],
                approval_license_type=record['approval_license_type']
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')
            numRecord += 1

    print("finished bronx")
    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()
    
def scrape_manhattan():
    url = 'https://health.data.ny.gov/resource/dmn7-mpa8.json?$query=SELECT%0A%20%20%60county%60%2C%0A%20%20%60site_name%60%2C%0A%20%20%60address_omitted%60%2C%0A%20%20%60street1%60%2C%0A%20%20%60street2%60%2C%0A%20%20%60city%60%2C%0A%20%20%60state%60%2C%0A%20%20%60zipcode%60%2C%0A%20%20%60program_type%60%2C%0A%20%20%60approval_license_type%60%2C%0A%20%20%60site_type%60%2C%0A%20%20%60breastfeeding_friendly_certified%60%2C%0A%20%20%60eat_well_play_hard_ccs_participant%60%2C%0A%20%20%60last_ewphccs_participation%60%2C%0A%20%20%60eat_well_play_hard_dch_participant%60%2C%0A%20%20%60last_ewphdch_participation%60%2C%0A%20%20%60location%60%0AWHERE%20caseless_one_of(%60county%60%2C%20%22MANHATTAN%22)%20AND%20(%60street1%60%20IS%20NOT%20NULL)'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    numRecord = 1
    for record in homebases:
        if numRecord <= 50:
            entry = models.Pantries(
                name=record['site_name'],
                borough=record['county'], 
                address=record['street1'],
                site_type=record['site_type'],
                program_type=record['program_type'],
                approval_license_type=record['approval_license_type']
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')
            numRecord += 1

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()
    
def scrape_queens():
    url = 'https://health.data.ny.gov/resource/dmn7-mpa8.json?$query=SELECT%0A%20%20%60county%60%2C%0A%20%20%60site_name%60%2C%0A%20%20%60address_omitted%60%2C%0A%20%20%60street1%60%2C%0A%20%20%60street2%60%2C%0A%20%20%60city%60%2C%0A%20%20%60state%60%2C%0A%20%20%60zipcode%60%2C%0A%20%20%60program_type%60%2C%0A%20%20%60approval_license_type%60%2C%0A%20%20%60site_type%60%2C%0A%20%20%60breastfeeding_friendly_certified%60%2C%0A%20%20%60eat_well_play_hard_ccs_participant%60%2C%0A%20%20%60last_ewphccs_participation%60%2C%0A%20%20%60eat_well_play_hard_dch_participant%60%2C%0A%20%20%60last_ewphdch_participation%60%2C%0A%20%20%60location%60%0AWHERE%20caseless_one_of(%60county%60%2C%20%22QUEENS%22)%20AND%20(%60street1%60%20IS%20NOT%20NULL)'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    numRecord = 1
    for record in homebases:
        if numRecord <= 50:
            entry = models.Pantries(
                name=record['site_name'],
                borough=record['county'], 
                address=record['street1'],
                site_type=record['site_type'],
                program_type=record['program_type'],
                approval_license_type=record['approval_license_type']
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')
            numRecord += 1

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()

def scrape_brooklyn():
    url = 'https://health.data.ny.gov/resource/dmn7-mpa8.json?$query=SELECT%0A%20%20%60county%60%2C%0A%20%20%60site_name%60%2C%0A%20%20%60address_omitted%60%2C%0A%20%20%60street1%60%2C%0A%20%20%60street2%60%2C%0A%20%20%60city%60%2C%0A%20%20%60state%60%2C%0A%20%20%60zipcode%60%2C%0A%20%20%60program_type%60%2C%0A%20%20%60approval_license_type%60%2C%0A%20%20%60site_type%60%2C%0A%20%20%60breastfeeding_friendly_certified%60%2C%0A%20%20%60eat_well_play_hard_ccs_participant%60%2C%0A%20%20%60last_ewphccs_participation%60%2C%0A%20%20%60eat_well_play_hard_dch_participant%60%2C%0A%20%20%60last_ewphdch_participation%60%2C%0A%20%20%60location%60%0AWHERE%20caseless_one_of(%60county%60%2C%20%22BROOKLYN%22)%20AND%20(%60street1%60%20IS%20NOT%20NULL)'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    numRecord = 1
    for record in homebases:
        if numRecord <= 50:
            entry = models.Pantries(
                name=record['site_name'],
                borough=record['county'], 
                address=record['street1'],
                site_type=record['site_type'],
                program_type=record['program_type'],
                approval_license_type=record['approval_license_type']
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')
            numRecord += 1

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()
    
def scrape_staten_island():
    url = 'https://health.data.ny.gov/resource/dmn7-mpa8.json?$query=SELECT%0A%20%20%60county%60%2C%0A%20%20%60site_name%60%2C%0A%20%20%60address_omitted%60%2C%0A%20%20%60street1%60%2C%0A%20%20%60street2%60%2C%0A%20%20%60city%60%2C%0A%20%20%60state%60%2C%0A%20%20%60zipcode%60%2C%0A%20%20%60program_type%60%2C%0A%20%20%60approval_license_type%60%2C%0A%20%20%60site_type%60%2C%0A%20%20%60breastfeeding_friendly_certified%60%2C%0A%20%20%60eat_well_play_hard_ccs_participant%60%2C%0A%20%20%60last_ewphccs_participation%60%2C%0A%20%20%60eat_well_play_hard_dch_participant%60%2C%0A%20%20%60last_ewphdch_participation%60%2C%0A%20%20%60location%60%0AWHERE%0A%20%20caseless_one_of(%60county%60%2C%20%22STATEN%20ISLAND%22)%20AND%20(%60street1%60%20IS%20NOT%20NULL)'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    homebases = response.json()

    entries = []
    numRecord = 1
    for record in homebases:
        if numRecord <= 50:
            entry = models.Pantries(
                name=record['site_name'],
                borough=record['county'], 
                address=record['street1'],
                site_type=record['site_type'],
                program_type=record['program_type'],
                approval_license_type=record['approval_license_type']
            )
            entries.append(entry)
            print(f'Scraped {entry.name}')
            numRecord += 1

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()

def create_pantries_model():
    scrape_bronx()
    scrape_brooklyn()
    scrape_manhattan()
    scrape_queens()
    scrape_staten_island()
