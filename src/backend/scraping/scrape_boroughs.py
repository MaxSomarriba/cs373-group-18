import requests
from .. import models
from ..config import settings
from ..db import SessionLocal
from .gmap_search import get_location_info

def create_boroughs_model():
    # associated address by borough and community district
    url = 'https://data.cityofnewyork.us/resource/ur7y-ziyb.json?$query=SELECT%0A%20%20%60report_date%60%2C%0A%20%20%60borough%60%2C%0A%20%20%60community_district%60%2C%0A%20%20%60case_type%60%2C%0A%20%20%60cases%60%2C%0A%20%20%60individuals%60%0AWHERE%0A%20%20(%60report_date%60%20%3D%20%222022-06-30T00%3A00%3A00%22%20%3A%3A%20floating_timestamp)%0A%20%20AND%20caseless_not_one_of(%60community_district%60%2C%20%22%22%2C%20%22Unknown%20CD%22)%0AORDER%20BY%20%60report_date%60%20DESC%20NULL%20FIRST'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    associated_address = response.json()

    # buildings by borough and community district
    url = 'https://data.cityofnewyork.us/resource/3qem-6v3v.json?$query=SELECT%0A%20%20%60report_date%60%2C%0A%20%20%60borough%60%2C%0A%20%20%60community_district%60%2C%0A%20%20%60adult_family_comm_hotel%60%2C%0A%20%20%60adult_family_shelter%60%2C%0A%20%20%60adult_shelter%60%2C%0A%20%20%60adult_shelter_comm_hotel%60%2C%0A%20%20%60fwc_cluster%60%2C%0A%20%20%60fwc_comm_hotel%60%2C%0A%20%20%60fwc_shelter%60%0AWHERE%0A%20%20caseless_not_one_of(%60borough%60%2C%20%22%22%2C%20%22Westchester%22)%0A%20%20AND%20(%60report_date%60%20%3D%20%222024-01-31T00%3A00%3A00%22%20%3A%3A%20floating_timestamp)%0AORDER%20BY%20%60report_date%60%20DESC%20NULL%20FIRST'
    headers = {
        'X-App-Token': settings.nyc_opendata_key
    }

    # Make a GET request with the specified headers
    response = requests.get(url, headers=headers)
    buildings = response.json()

    # associated_address has all 59 cds while buildings is missing a few
    # will set missing values from buildings to null
    entries = []
    buildings_index = 0
    for record in associated_address: # has all 59 cds
        adult_buildings = 0
        family_buildings = 0
        fwc_buildings = 0

        building = buildings[buildings_index]
        if int(record['community_district']) == (int(building['community_district']) % 100):
            adult_buildings = int(building.get('adult_shelter_comm_hotel', 0)) + int(building.get('adult_shelter', 0))
            family_buildings = int(building.get('adult_family_comm_hotel', 0)) + int(building.get('adult_family_shelter', 0))
            fwc_buildings = int(building.get('fwc_comm_hotel', 0)) + int(building.get('fwc_shelter', 0))
            buildings_index += 1 if buildings_index != len(buildings) - 1 else 0
            # if cd doesn't match, then don't move up building record bc next record is higher up

        entry = models.Boroughs(
            borough=record['borough'],
            community_district_num=record['community_district'],
            adult_buildings=adult_buildings,
            family_buildings=family_buildings,
            fwc_buildings=fwc_buildings,
            individual_count=record['individuals'],
            cases_count=record['cases']
        )
        entries.append(entry)
        print(f'Scraped {entry.borough} community district {entry.community_district_num}')

    db = SessionLocal()
    db.add_all(entries)
    db.commit()
    db.close()
