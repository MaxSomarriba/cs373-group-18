import requests
from ..config import settings
from .. import models
from ..db import SessionLocal

def get_street_view_image(location):
    base_url = "https://maps.googleapis.com/maps/api/streetview"
    params = {
        'location': location,
        'size': '640x640',
        'key': settings.google_maps_api_key
    }

    response = requests.get(base_url, params=params)
    
    if response.status_code == 200:
        return response.url
    else:
        print(f"Error fetching Street View image for {location}. Status Code: {response.status_code}")
        return None

def scrape_shelter_images():
    db = SessionLocal()
    shelters = db.query(models.Shelters).all()
    
    for shelter in shelters:
        location = f"nyc {shelter.address}"
        image_url = get_street_view_image(location)

        if image_url:
            shelter.image_url = image_url
            db.commit()
            print(f'Scraped Street View image for {shelter.name}')
        else:
            print(f'No Street View image found for {shelter.name}')

    db.close()

def scrape_borough_images():
    db = SessionLocal()
    boroughs = db.query(models.Boroughs).all()

    for borough in boroughs:
        location = f"nyc {borough.borough} Community District {borough.community_district_num}"
        image_url = get_street_view_image(location)

        if image_url:
            borough.image_url = image_url
            db.commit()
            print(f'Scraped Street View image for {borough.borough} community district {borough.community_district_num}')
        else:
            print(f'No Street View image found for {borough.borough} community district {borough.community_district_num}')

    db.close()

def scrape_food_images():
    db = SessionLocal()
    foods = db.query(models.Pantries).all()

    for food in foods:
        location = f"nyc {food.address}"
        image_url = get_street_view_image(location)

        if image_url:
            food.image_url = image_url
            db.commit()
            print(f'Scraped Street View image for {food.name}')
        else:
            print(f'No Street View image found for {food.name}')

    db.close()