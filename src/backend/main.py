from fastapi import FastAPI, APIRouter, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from .routers import shelters, pantries, boroughs
from .config import settings
from . import models
from .db import engine

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(shelters.router)
app.include_router(pantries.router)
app.include_router(boroughs.router)

templates = Jinja2Templates(directory='backend/templates')

@app.get("/", response_class=HTMLResponse)
def hello(request: Request):
        return templates.TemplateResponse('hello.html', {"request": request})

if __name__ == '__main__':
    app.run(host = "0.0.0.0", port = 5000, debug = True)
