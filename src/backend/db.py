from sqlalchemy import create_engine # to create database
from sqlalchemy.ext.declarative import declarative_base # all models inherit from this as base class
from sqlalchemy.orm import sessionmaker
from .config import settings

DATABASE_URL = f"mysql+pymysql://{settings.db_user}:{settings.db_password}@{settings.db_host}:{settings.db_port}/{settings.db_database}"
## get database
engine = create_engine(DATABASE_URL)
Base = declarative_base()

# engine = create_engine("sqlite:///src/mydb.db", echo=True)
# make sure db is in src folder! this was for local testing^

# attach database to local session (so i can make/commit changes)
SessionLocal = sessionmaker(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()