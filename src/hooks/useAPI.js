import { useEffect, useState } from "react";
import axios from "axios";

const useAPI = (endpoint, requestConfig, deps) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(
    () => {
      setLoading(true);
      const source = axios.CancelToken.source();

      axios
        .get(`${'https://api.homehopeny.site'}${endpoint}`, {
          cancelToken: source.token, 
          ...requestConfig, headers: {'Referrer-Policy' : "unsafe_url"}
        })
        .then((res) => {
          setData(res.data);
          setLoading(false);
        })
        .catch((err) => {
          if (axios.isCancel(err)) return;
          setError(err.message);
          setLoading(false);
        });

      return () => source.cancel();
    },
    deps ? [...deps] : []
  );

  return { data, error, loading };
};

export default useAPI;
