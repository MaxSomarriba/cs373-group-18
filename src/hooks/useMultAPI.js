import { useEffect, useState } from "react";
import axios from "axios";

const useMultAPI = (endpoint, requestConfig, deps) => {
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(
    () => {
      setLoading(true);
      const source = axios.CancelToken.source();

      axios
        .get(`${'https://api.homehopeny.site/Boroughs'}`, {
          cancelToken: source.token, 
          ...requestConfig, headers: {'Referrer-Policy' : "unsafe_url"}
        })
        .then((res) => {
          setData(res.data);
          setLoading(false);
        })
        .catch((err) => {
          if (axios.isCancel(err)) return;
          setError(err.message);
          setLoading(false);
        });

      return () => source.cancel();
    },
    deps ? [...deps] : []
  );

  useEffect(
    () => {
      setLoading(true);
      const source = axios.CancelToken.source();

      axios
        .get(`${'https://api.homehopeny.site/Shelters'}`, {
          cancelToken: source.token, 
          ...requestConfig, headers: {'Referrer-Policy' : "unsafe_url"}
        })
        .then((res) => {
          setData2(res.data);
          setLoading(false);
        })
        .catch((err) => {
          if (axios.isCancel(err)) return;
          setError(err.message);
          setLoading(false);
        });

      return () => source.cancel();
    },
    deps ? [...deps] : []
  );

  useEffect(
    () => {
      setLoading(true);
      const source = axios.CancelToken.source();

      axios
        .get(`${'https://api.homehopeny.site/Pantries'}`, {
          cancelToken: source.token, 
          ...requestConfig, headers: {'Referrer-Policy' : "unsafe_url"}
        })
        .then((res) => {
          setData3(res.data);
          setLoading(false);
        })
        .catch((err) => {
          if (axios.isCancel(err)) return;
          setError(err.message);
          setLoading(false);
        });

      return () => source.cancel();
    },
    deps ? [...deps] : []
  );

  return { data, data2, data3, error, loading };
};

export default useMultAPI;
