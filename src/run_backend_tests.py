import unittest
from backend.API.testing.tests import TestAPIEndpoints

if __name__ == "__main__":
    # Load test cases from TestAPIEndpoints class
    test_cases = unittest.TestLoader().loadTestsFromTestCase(TestAPIEndpoints)

    # Create a test suite
    test_suite = unittest.TestSuite(test_cases)

    # Run the test suite
    unittest.TextTestRunner().run(test_suite)
