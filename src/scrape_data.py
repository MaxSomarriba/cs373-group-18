from backend.scraping.scrape_shelters import create_shelters_model
from backend.scraping.scrape_pantries import create_pantries_model
from backend.scraping.scrape_boroughs import create_boroughs_model
from backend.scraping.images import scrape_shelter_images, scrape_borough_images, scrape_food_images
from backend import models
from backend.db import engine
from backend import main

if __name__ == '__main__':
    print('pls work')
    # models.Base.metadata.create_all(bind=engine)
    # create_shelters_model()
    # scrape_shelter_images()
    # print(f'\n\n\nSHELTERS CREATED\n\n\n')
    
    # create_pantries_model()
    # scrape_food_images()
    # print(f'\n\n\nPANTRIES CREATED\n\n\n')

    # create_boroughs_model()
    # scrape_borough_images()
    # print(f'\n\n\nBOROUGHS CREATED\n\n\n')