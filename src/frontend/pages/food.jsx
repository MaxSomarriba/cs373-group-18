import React, { useState, useEffect } from "react";

import FoodPantriesCard from "../components/cards/FoodPantriesCard";
import CardGrid from "../components/CardGrid";
import useAPI from "../../hooks/useAPI";
import SearchBar from "../components/SearchBar";
import SortSelector from "../components/selectors/SortSelector";
import OrderSelector from "../components/selectors/OrderSelector";

import { Box, Heading, HStack, Center, Select } from "@chakra-ui/react";

const CARD_LIMIT = 24;

const FoodPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [query, setQuery] = useState({});
  const [selectedBorough, setSelectedBorough] = useState("");
  const [selectedApprovalLicenseType, setSelectedApprovalLicenseType] =
    useState("");
  const [selectedSiteType, setSelectedSiteType] = useState("");

  const { data, error, loading } = useAPI(
    "/Pantries",
    {
      params: {
        limit: CARD_LIMIT,
        skip: (currentPage - 1) * CARD_LIMIT,
        ...query,
      },
    },
    [currentPage, query]
  );

  const foodpantries = data?.results;
  const totalResults = data?.count;

  return (
    <>
      <Box mt={8} mx="auto" maxWidth="container.xl">
        <Center mb={4}>
          <Heading
            as="h1"
            fontWeight="medium"
            fontFamily="DM Serif Display, serif"
            fontSize="40px"
          >
            Food Pantries
          </Heading>
        </Center>
        <Center mb={2}>
          <p
            style={{ fontSize: "23px", fontFamily: "DM Serif Display, serif" }}
          >
            Total Results: {totalResults}
          </p>
        </Center>

        <Center mb={10}>
          <Select
            placeholder="Select Borough"
            value={selectedBorough}
            onChange={(e) => {
              const borough = e.target.value;
              setSelectedBorough(borough);
              setQuery({ ...query, borough });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="Brooklyn">Brooklyn</option>
            <option value="Queens">Queens</option>
            <option value="Manhattan">Manhattan</option>
            <option value="Bronx">Bronx</option>
            <option value="Staten Island">Staten Island</option>
          </Select>
        </Center>

        <Center mb={10}>
          <Select
            placeholder="Select License"
            value={selectedApprovalLicenseType}
            onChange={(e) => {
              const approval_license_type = e.target.value;
              setSelectedApprovalLicenseType(approval_license_type);
              setQuery({ ...query, approval_license_type });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="UNLICENSED: SCHOOL-RUN">Unlicensed</option>
            <option value="NYC DEPARTMENT OF HEALTH AND MENTAL HYGIENE">
              NYC Department of health and mental hygiene
            </option>
            <option value="OFFICE OF CHILDREN AND FAMILY SERVICES">
              Office of children and family services
            </option>
          </Select>
        </Center>

        <Center mb={10}>
          <Select
            placeholder="Select Site Type"
            value={selectedSiteType}
            onChange={(e) => {
              const site_type = e.target.value;
              setSelectedSiteType(site_type);
              setQuery({ ...query, site_type });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="SCHOOL AGE">School Age</option>
            <option value="CHILD CARE">Child Care</option>
          </Select>
        </Center>

        <HStack spacing={4} mb={8}>
          <SearchBar
            onSearch={(input) => {
              setQuery({ ...query, search: input });
              setCurrentPage(1);
            }}
          />
          <SortSelector
            sortOptions={[
              { value: "name", label: "Name" },
              { value: "borough", label: "Borough" },
              { value: "site_type", label: "Site Type" },
              {
                value: "approval_license_type",
                label: "Approval License Type",
              },
              { value: "address", label: "Address" },
            ]}
            selectedSort={query.sort}
            onSelect={(sort) => {
              if (sort === null) {
                setQuery({ ...query, sort, order: null });
              } else {
                setQuery({ ...query, sort });
              }
              setCurrentPage(1);
            }}
          />
          <OrderSelector
            selectedOrder={query.order}
            onSelect={(order) => {
              setQuery({ ...query, order });
              setCurrentPage(1);
            }}
          />
        </HStack>

        <CardGrid
          CardComponent={FoodPantriesCard}
          modelData={foodpantries}
          totalResults={totalResults}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={CARD_LIMIT}
          searchValue={query.search}
        />
      </Box>
    </>
  );
};

export default FoodPage;
