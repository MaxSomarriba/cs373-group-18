import React, { useState, useEffect } from "react";
import axios from "axios";

import CityCard from "../components/cards/CityCard";
import FoodPantriesCard from "../components/cards/FoodPantriesCard";
import ShelterCard from "../components/cards/ShelterCard";
import CardGrid from "../components/CardGrid";
import boroughDataBackup from "../../hooks/boroughData.json";
import useAPI from "../../hooks/useMultAPI";
import SearchBar from "../components/SearchBar";
import FilterSelector from "../components/selectors/FilterSelector";
import SortSelector from "../components/selectors/SortSelector";

import { Box, Heading, HStack, Center } from "@chakra-ui/react";

const CARD_LIMIT = 24;

const SearchPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [query, setQuery] = useState({});

  const { data, data2, data3, error, loading } = useAPI(
    "/Boroughs",
    {
      params: {
        limit: 10,
        skip: (currentPage - 1) * CARD_LIMIT,
        ...query,
      },
    },
    [currentPage, query]
  );
  console.log("data:");
  console.log(data);
  console.log(data2);
  console.log(boroughDataBackup);

  const boroughs = data?.results;
  const shelters = data2?.results;
  const pantries = data3?.results;

  const bTotal = data?.count;
  const sTotal = data2?.count;
  const pTotal = data3?.count;

  console.log("boroughs:");
  console.log(boroughs);

  const totalResults = bTotal + sTotal + pTotal;
  console.log(totalResults);

  return (
    <>
      <Box mt={8} mx="auto" maxWidth="container.xl">
        <Center mb={4}>
          <Heading
            as="h1"
            fontWeight="medium"
            fontFamily="DM Serif Display, serif"
            fontSize="40px"
          >
            Global Search
          </Heading>
        </Center>
        <Center mb={2}>
          <p
            style={{ fontSize: "23px", fontFamily: "DM Serif Display, serif" }}
          >
            Total Results: {totalResults}
          </p>
        </Center>

        <HStack spacing={4} mb={8}>
          <SearchBar
            onSearch={(input) => {
              setQuery({ ...query, search: input });
              setCurrentPage(1);
            }}
          />
          <SortSelector
            sortOptions={[
              { value: "name", label: "Name" },
              { value: "population", label: "Population" },
            ]}
            selectedSort={query.sort}
            onSelect={(sort) => {
              if (sort === null) {
                setQuery({ ...query, sort, order: null });
              } else {
                setQuery({ ...query, sort });
              }
              setCurrentPage(1);
            }}
          />
        </HStack>
        <Heading
          as="h1"
          fontWeight="medium"
          fontFamily="DM Serif Display, serif"
          fontSize="40px"
        >
          <Center>Boroughs</Center>
        </Heading>
        <CardGrid
          CardComponent={CityCard}
          modelData={boroughs}
          totalResults={bTotal}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={10}
          searchValue={query.search}
        />

        <Heading
          as="h1"
          fontWeight="medium"
          fontFamily="DM Serif Display, serif"
          fontSize="40px"
        >
          <Center>Shelters</Center>
        </Heading>
        <CardGrid
          CardComponent={ShelterCard}
          modelData={shelters}
          totalResults={sTotal}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={10}
          searchValue={query.search}
        />

        <Heading
          as="h1"
          fontWeight="medium"
          fontFamily="DM Serif Display, serif"
          fontSize="40px"
        >
          <Center>Pantries</Center>
        </Heading>
        <CardGrid
          CardComponent={FoodPantriesCard}
          modelData={pantries}
          totalResults={pTotal}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={10}
          searchValue={query.search}
        />
      </Box>
    </>
  );
};

export default SearchPage;
