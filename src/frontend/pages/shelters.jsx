import React, { useState, useEffect } from "react";
import ShelterCard from "../components/cards/ShelterCard";
import CardGrid from "../components/CardGrid";
import useAPI from "../../hooks/useAPI";
import SearchBar from "../components/SearchBar";
import SortSelector from "../components/selectors/SortSelector";
import OrderSelector from "../components/selectors/OrderSelector";

import { Box, Heading, HStack, Center, Image, Select } from "@chakra-ui/react";

const CARD_LIMIT = 12;

const SheltersPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [query, setQuery] = useState({});
  const [selectedBorough, setSelectedBorough] = useState("");
  const [selectedProgramType, setSelectedProgramType] = useState("");

  const { data, error, loading } = useAPI(
    "/Shelters",
    {
      params: {
        limit: CARD_LIMIT,
        skip: (currentPage - 1) * CARD_LIMIT,
        ...query,
      },
    },
    [currentPage, query]
  );

  const shelters = data?.results;
  const totalResults = data?.count;

  return (
    <>
      <Box mt={8} mx="auto" maxWidth="container.xl">
        <Center mb={4}>
          <Heading
            as="h1"
            fontWeight="medium"
            fontFamily="DM Serif Display, serif"
            fontSize="40px"
          >
            Shelters
          </Heading>
        </Center>
        <Center mb={2}>
          <p
            style={{ fontSize: "23px", fontFamily: "DM Serif Display, serif" }}
          >
            Total Results: {totalResults}
          </p>
        </Center>
        <Center mb={10}>
          <Select
            placeholder="Select Borough"
            value={selectedBorough}
            onChange={(e) => {
              const borough = e.target.value;
              setSelectedBorough(borough);
              setQuery({ ...query, borough });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="Brooklyn">Brooklyn</option>
            <option value="Queens">Queens</option>
            <option value="Manhattan">Manhattan</option>
            <option value="Bronx">Bronx</option>
            <option value="Staten Island">Staten Island</option>
          </Select>
        </Center>
        <Center mb={10}>
          <Select
            placeholder="Select Program Type"
            value={selectedProgramType}
            onChange={(e) => {
              const program_type = e.target.value;
              setSelectedProgramType(program_type);
              setQuery({ ...query, program_type });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="FAMILY">Family</option>
            <option value="YOUTH">Youth</option>
            <option value="ADULT">Adult</option>
            <option value="HOMEBASE">Homebase</option>
          </Select>
        </Center>

        <HStack spacing={4} mb={8}>
          <SearchBar
            onSearch={(input) => {
              setQuery({ ...query, search: input });
              setCurrentPage(1);
            }}
          />
          <SortSelector
            sortOptions={[
              { value: "name", label: "Name" },
              { value: "borough", label: "Borough" },
              { value: "program_type", label: "Program Type" },
              { value: "address", label: "Address" },
              { value: "phone", label: "Phone" },
            ]}
            selectedSort={query.sort}
            onSelect={(sort) => {
              if (sort === null) {
                setQuery({ ...query, sort, order: null });
              } else {
                setQuery({ ...query, sort });
              }
              setCurrentPage(1);
            }}
          />

          <OrderSelector
            selectedOrder={query.order}
            onSelect={(order) => {
              setQuery({ ...query, order });
              setCurrentPage(1);
            }}
          />
        </HStack>

        <CardGrid
          CardComponent={ShelterCard}
          modelData={shelters}
          totalResults={totalResults}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={CARD_LIMIT}
          searchValue={query.search}
        />
      </Box>
    </>
  );
};

export default SheltersPage;
