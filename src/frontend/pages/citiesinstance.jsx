import { React, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Box,
  Center,
  Grid,
  GridItem,
  Text,
  Image,
  Heading,
  Link,
  List,
  ListIcon,
  ListItem,
} from "@chakra-ui/react";
import { getGoogleMapsUrl } from "../services/googleMaps";
import InstanceAttributeCard from "../components/cards/InstanceAttributeCard";
import useAPI from "../../hooks/useAPI";
import boroughDataBackup from "../../hooks/boroughData.json";
import nycFoodPantriesDataBackup from "../../hooks/nycFoodPantries.json";
import nycShelterDataBackup from "../../hooks/nycShelterData.json";

const BoroughInstance = () => {
  const { id } = useParams();
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  const {
    data: sheltersRes,
    error: sheltersError,
    loading: sheltersLoading,
  } = useAPI("/Shelters", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: pantriesRes,
    error: pantriesError,
    loading: pantriesLoading,
  } = useAPI("/Pantries", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: boroughRes,
    error: boroughError,
    loading: boroughLoading,
  } = useAPI(`/Boroughs/${id}`);

  // Combine loading states
  const loading = boroughLoading || pantriesLoading || sheltersLoading;

  if (loading) {
    return <div>Loading...</div>;
  }

  const pantries = pantriesRes
    ? pantriesRes?.results
    : nycFoodPantriesDataBackup;
  const shelters = sheltersRes ? sheltersRes?.results : nycShelterDataBackup;
  const boroughcd = boroughRes
    ? boroughRes
    : boroughDataBackup.find((b) => b.id === parseInt(id));
  const url = getGoogleMapsUrl(boroughcd.name);

  return (
    <>
      {
        <Box p={8} mx="auto" maxWidth="container.xl">
          <Center flexDirection="column" mb={6}>
            <Image
              src={imageErr ? alternateImage : boroughcd.image_url}
              alt={boroughcd.borough}
              objectFit="contain"
              width="400px"
              height="250px"
              mb={4}
              onError={() => setImageErr(true)}
            />
          </Center>

          <Grid templateColumns="2fr 1fr" gap={8} mb={12}>
            <Box>
              {/* provide as title */}
              <Heading as="h2" size="xl" color="Orange" mb={4}>
                {boroughcd.borough}
              </Heading>
              {/* community district type as subtitle */}
              <Text mt={4} fontSize="x-large">
                Community District {boroughcd.community_district_num}
              </Text>

              {/* links to other instances */}
              <Grid templateColumns="repeat(2, 1fr)" gap={6} mt={12}>
                {shelters && shelters.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Shelters in {boroughcd.borough}
                    </Heading>
                    <List spacing={2}>
                      {shelters
                        .filter((shelter) =>
                          shelter.borough
                            .toLowerCase()
                            .includes(boroughcd.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((shelter) => (
                          <ListItem key={shelter.id}>
                            <Link
                              href={`/homelessshelters/${shelter.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              {shelter.name}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}

                {pantries && pantries.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Pantries in {boroughcd.borough}
                    </Heading>
                    <List spacing={2}>
                      {pantries
                        .filter((pantry) =>
                          pantry.borough
                            .toLowerCase()
                            .includes(boroughcd.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((pantry) => (
                          <ListItem key={pantry.id}>
                            <Link
                              href={`/foodpantriesandkitchens/${pantry.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              {pantry.name}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}
              </Grid>
              <InstanceAttributeCard {...boroughcd} />
              <iframe
                width="auto"
                height="380"
                frameborder="0"
                styles="border:0;overflow:auto;"
                referrerpolicy="no-referrer-when-downgrade"
                src={url}
                allowfullscreen
              ></iframe>
              <a href="https://www.coalitionforthehomeless.org/donate/">
                <button
                  style={{
                    backgroundColor: "Orange",
                    color: "white",
                    padding: "10px 20px",
                    border: "none",
                    borderRadius: "5px",
                    fontWeight: "bold",
                    cursor: "pointer",
                    fontFamily: "DM Serif Display, serif",
                    margin: "20px",
                  }}
                >
                  Link to Donate!
                </button>
              </a>
            </Box>
          </Grid>
        </Box>
      }
    </>
  );
};

export default BoroughInstance;
