import {
  Center,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Heading,
  Box,
} from "@chakra-ui/react";
import ShelterPieChart from "../components/visualizations/ShelterPieChart";
import ShelterProgramTypeBarChart from "../components/visualizations/ShelterProgramTypeBarChart";
import PantriesLiscenseTypeBubbleChart from "../components/visualizations/PantriesLiscenseTypeBubbleChart";
import TreatmentBubbleChart from "../components/visualizations/TreatmentBubble";
import TreatmentCenterBarGraph from "../components/visualizations/TreatmentCentersBarGraph";
import DiseasePieChart from "../components/visualizations/DiseasePieChart";

const Visualizations = () => {
  return (
    <>
      <Box mt={8} mx="auto" maxWidth="container.xl"></Box>
      <Center mb={4}>
        <Heading as="h1" fontWeight="medium">
          Visualizations
        </Heading>
      </Center>

      <Tabs isFitted size="lg" colorScheme="teal">
        <TabList mb="1em" mt="1em">
          <Tab>HomeHope New York</Tab>
          <Tab>Autoimmune Diseases</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <Center flexDirection="column">
              <Heading as="h2" fontWeight="medium" mb={6}>
                Shelter's Per Borough
              </Heading>
              <ShelterPieChart />
              <Heading as="h2" fontWeight="medium" mb={6} mt={12}>
                Program Type Per Shelter
              </Heading>
              <ShelterProgramTypeBarChart />
              <Heading as="h2" fontWeight="medium" mb={6} mt={12}>
                Liscense Type Per Pantry
              </Heading>
              <PantriesLiscenseTypeBubbleChart />
              <Heading>Critiques </Heading>
              <b>What did we do well?</b>
              We communicated well with each other. We kept each other updated
              on our availability, as well as the parts of the code that we were
              working on. We also helped each other out whenever there were
              issues that arose.
              <b>What did we learn?</b>
              We learned a lot about software development procedures and how to
              coordinate the development of a website between 5 people
              synchronously working on different aspects of the project.
              <b>What did we teach each other?</b>
              We taught each other how to be patient and to support one another
              while we are working on the website.
              <b>What can we do better?</b>
              We could have done a better job of focussing on UI design from the
              very beginning. Initially our website was very ugly until we went
              and fixed it.
              <b>What effect did the peer reviews have?</b>
              It helped us in ensuring everyone was on the same track as well as
              what we could all improve on in the future. It also reassured us
              about what we needed to continue doing in order to be successful
              in completing our goals for each phase.
              <b>What puzzles us? </b>
              We are still puzzled how AWS is profitable after giving us so many
              free credits for our server. We are very happy with the end
              product.
            </Center>
          </TabPanel>

          <TabPanel>
            <Center flexDirection="column">
              <Heading as="h2" fontWeight="medium" mb={6}>
                Treatment Success Rate vs Cost
              </Heading>
              <TreatmentBubbleChart></TreatmentBubbleChart>
              <Heading as="h2" fontWeight="medium" mb={6} mt={12}>
                Treatment Center Count vs City
              </Heading>
              <TreatmentCenterBarGraph></TreatmentCenterBarGraph>
              <Heading as="h2" fontWeight="medium" mb={6} mt={12}>
                Diseases by Rarity
              </Heading>
              <DiseasePieChart></DiseasePieChart>
              <Heading>Critiques </Heading>
              <b>What did they do well?</b>
              They communicated very well with us over slack and the
              conversation on the issues and modifying them was done very well.
              They also implemented our user stories very well.
              <b>How effective was their RESTful API?</b>
              The RESTful API was done very well. The documentation was complete
              and easy to read. It was also very easy to access the data from
              their API.
              <b>How well did they implement your user stories?</b>
              Very well. A lot of our user stories were very ambitious but they
              did a good job of doing them to an amazing extent.
              <b>What did we learn from their website?</b>
              We learned a lot about the different autoimmune diseases and
              treatments available for each particular disease. It was also nice
              to see the different ways the frontend of a website could be
              implemented.
              <b>What can they do better?</b>
              They can focus on UI design and aim to make the website more
              cohesive and look better and more professional. Many websites use
              transitions,CSS effects, and cohesive colors to make the website
              feel like parts of a whole.
              <b>What puzzles us about their website?</b>I didn't understand why
              they used such bright colors for everything, it was very
              distracting and at times could take away from our ability to
              interact with the site and focus on the data they were presenting.
            </Center>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
};

export default Visualizations;
