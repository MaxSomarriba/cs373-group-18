import React from "react";
import GoogleMapNameToAddress from "../../backend/scraping/GoogleMapNameToAddress";

const Test = () => {
  return (
    <div>
      <GoogleMapNameToAddress locationName="Empire State Building" />
    </div>
  );
};
export default Test;
