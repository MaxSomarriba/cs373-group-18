import React, { useState, useEffect } from "react";

import CityCard from "../components/cards/CityCard";
import CardGrid from "../components/CardGrid";
import boroughDataBackup from "../../hooks/boroughData.json";
import useAPI from "../../hooks/useAPI";
import SearchBar from "../components/SearchBar";
import SortSelector from "../components/selectors/SortSelector";
import OrderSelector from "../components/selectors/OrderSelector";

import { Box, Heading, HStack, Center, Select } from "@chakra-ui/react";
const CARD_LIMIT = 24;

const CitiesPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [query, setQuery] = useState({});
  const [selectedBorough, setSelectedBorough] = useState("");
  const { data, error, loading } = useAPI(
    "/Boroughs",
    {
      params: {
        limit: CARD_LIMIT,
        skip: (currentPage - 1) * CARD_LIMIT,
        ...query,
      },
    },
    [currentPage, query]
  );

  console.log("data:");
  console.log(data);
  console.log(boroughDataBackup);

  const boroughs = data?.results;
  console.log("boroughs:");
  console.log(boroughs);

  const totalResults = data?.count;
  console.log(totalResults);

  return (
    <>
      <Box mt={8} mx="auto" maxWidth="container.xl">
        <Center mb={4}>
          <Heading
            as="h1"
            fontWeight="medium"
            fontFamily="DM Serif Display, serif"
            fontSize="40px"
          >
            Boroughs & Community Districts
          </Heading>
        </Center>
        <Center mb={2}>
          <p
            style={{ fontSize: "23px", fontFamily: "DM Serif Display, serif" }}
          >
            Total Results: {totalResults}
          </p>
        </Center>
        <Center mb={10}>
          <Select
            placeholder="Select Borough"
            value={selectedBorough}
            onChange={(e) => {
              const borough = e.target.value;
              setSelectedBorough(borough);
              setQuery({ ...query, borough });
              setCurrentPage(1);
            }}
            fontSize={14}
          >
            <option value="Brooklyn">Brooklyn</option>
            <option value="Queens">Queens</option>
            <option value="Manhattan">Manhattan</option>
            <option value="Bronx">Bronx</option>
            <option value="Staten Island">Staten Island</option>
          </Select>
        </Center>
        <HStack spacing={4} mb={8}>
          <SearchBar
            onSearch={(input) => {
              setQuery({ ...query, search: input });
              setCurrentPage(1);
            }}
          />
          <SortSelector
            sortOptions={[
              { value: "name", label: "Name" },
              {
                value: "community_district_num",
                label: "Community District Number",
              },
              { value: "adult_buildings", label: "Adult Buildings" },
              { value: "family_buildings", label: "Family Buildings" },
              { value: "fwc_buildings", label: "FWC Buildings" },
              { value: "individual_count", label: "Individual Count" },
              { value: "cases_count", label: "Cases Count" },
            ]}
            selectedSort={query.sort}
            onSelect={(sort) => {
              if (sort === null) {
                setQuery({ ...query, sort, order: null });
              } else {
                setQuery({ ...query, sort });
              }
              setCurrentPage(1);
            }}
          />
          <OrderSelector
            selectedOrder={query.order}
            onSelect={(order) => {
              setQuery({ ...query, order });
              setCurrentPage(1);
            }}
          />
        </HStack>

        <CardGrid
          CardComponent={CityCard}
          modelData={boroughs}
          totalResults={totalResults}
          currentPage={currentPage}
          onPageChange={(newPage) => setCurrentPage(newPage)}
          cardLimit={CARD_LIMIT}
          searchValue={query.search}
        />
      </Box>
    </>
  );
};

export default CitiesPage;
