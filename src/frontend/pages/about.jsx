import React, { useState, useEffect } from "react";
import axios from "axios";
import GitLabStats from "../components/about/GitLab";
import "./about.css";
import { Container, Row, Col } from "react-bootstrap";
import { Card } from "react-bootstrap";
import gitlab from "../../pictures/gitlab.svg";
import react from "../../pictures/react.svg";
import postman from "../../pictures/postman.svg";
import aws from "../../pictures/aws.svg";
import docker from "../../pictures/docker.svg";
import sel from "../../pictures/sel.svg";
import mysql from "../../pictures/mysql.svg";
import jest from "../../pictures/jest.svg";

export default function AboutPage() {
  return (
    <div className="About">
      <Container className="items-center text-center mt-2">
        <h1
          style={{ fontFamily: "DM Serif Display, serif", textAlign: "center" }}
        >
          About Home Hope
        </h1>
      </Container>
      <Container>
        <Row>
          <Col xs={12} md={6}>
            <p>
              The homeless population, regardless of location, has been one of
              the most underserved communities throughout the US despite also
              being one of the most well-known commmunities. This site focuses
              specifically on the homeless popoulation in NYC, looking to
              provide both the homeless population, or just a curious
              individual, a centralized way to get help and stay informed. We
              used data from homeless shelters, pantries, as well as
              geographical data to curate these resources in this website.
              Surprisingly, we found that there seems to be a large disparity
              between the number of shelters compared to food pantries
              throughout NYC despite the very large homeless popultion that
              resides there. Overall, The site works to bridge the gap between
              people who need resources but may not have the technical knowledge
              in order to find and locate the different websites with the data
              they need. The tools we used to make this website include React
              JS, Bootstrap, AWS Amplify, Postman, Chakra, Slack, GitLab, GitLab
              API.
              <br />
              <br />
            </p>
          </Col>
          <Col xs={12} md={6}>
            <img
              src="https://live.staticflickr.com/3450/3313566370_26e702c601_b.jpg"
              width={450}
            ></img>
          </Col>
        </Row>
      </Container>
      <br />
      <br />

      <Container className="items-center justify-center text-center">
        <div className="GitLabStatsContainer">
          <GitLabStats />
        </div>
      </Container>
      <Row style={{ paddingTop: "10px" }}>
        <Col className="text-center">
          <h2
            style={{
              fontWeight: "bold",
              fontSize: "45px",
              fontFamily: "DM Serif Display, serif",
            }}
          >
            Tools Used
          </h2>
        </Col>
      </Row>
      <Row
        style={{
          paddingTop: "30px",
          paddingBottom: "50px",
          paddingLeft: "30px",
          paddingRight: "30px",
        }}
      >
        <Col>
          <Card
            style={{
              border: "2px solid #000000",
              paddingLeft: "10px",
              margin: "2rem",
            }}
          >
            <Card.Img
              variant="top"
              src={gitlab}
              style={{
                width: "100%",
                height: "200px",
                objectFit: "scale-down",
              }}
            />
            <Card.Text
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontFamily: "DM Serif Display, serif",
                paddingTop: "1rem",
              }}
            >
              Gitlab
            </Card.Text>
          </Card>
          <Card style={{ border: "2px solid #000000" }}>
            <Card.Img
              variant="top"
              src={react}
              style={{
                width: "100%",
                height: "200px",
                objectFit: "scale-down",
              }}
            />
            <Card.Text
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontFamily: "DM Serif Display, serif",
                paddingTop: "1rem",
              }}
            >
              React
            </Card.Text>
          </Card>
          <Card style={{ border: "2px solid #000000" }}>
            <Card.Img
              variant="top"
              src={postman}
              style={{
                width: "100%",
                height: "200px",
                objectFit: "scale-down",
              }}
            />
            <Card.Text
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontFamily: "DM Serif Display, serif",
                paddingTop: "1rem",
              }}
            >
              Postman
            </Card.Text>
          </Card>
          <Card style={{ border: "2px solid #000000", margin: "2rem" }}>
            <Card.Img
              variant="top"
              src={aws}
              style={{
                width: "100%",
                height: "200px",
                objectFit: "scale-down",
              }}
            />
            <Card.Text
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontFamily: "DM Serif Display, serif",
                paddingTop: "1rem",
              }}
            >
              AWS
            </Card.Text>
          </Card>
          <Card style={{ border: "2px solid #000000" }}>
            <Card.Img
              variant="top"
              src={docker}
              style={{
                width: "100%",
                height: "200px",
                objectFit: "scale-down",
              }}
            />
            <Card.Text
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontFamily: "DM Serif Display, serif",
                paddingTop: "1rem",
              }}
            >
              Docker
            </Card.Text>
          </Card>
        </Col>

        <Container
          fluid
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Col>
            <Card style={{ border: "2px solid #000000", paddingLeft: "20px" }}>
              <Card.Img
                variant="top"
                src={mysql}
                style={{
                  width: "100%",
                  height: "200px",
                  objectFit: "scale-down",
                }}
              />
              <Card.Text
                style={{
                  textAlign: "center",
                  fontSize: "20px",
                  fontFamily: "DM Serif Display, serif",
                  paddingTop: "1rem",
                }}
              >
                MySql
              </Card.Text>
            </Card>

            <Card style={{ border: "2px solid #000000" }}>
              <Card.Img
                variant="top"
                src={jest}
                style={{
                  width: "100%",
                  height: "200px",
                  objectFit: "scale-down",
                }}
              />
              <Card.Text
                style={{
                  textAlign: "center",
                  fontSize: "20px",
                  fontFamily: "DM Serif Display, serif",
                  paddingTop: "1rem",
                }}
              >
                Jest
              </Card.Text>
            </Card>
            <Card style={{ border: "2px solid #000000" }}>
              <Card.Img
                variant="top"
                src={sel}
                style={{
                  width: "100%",
                  height: "200px",
                  objectFit: "scale-down",
                }}
              />
              <Card.Text
                style={{
                  textAlign: "center",
                  fontSize: "20px",
                  fontFamily: "DM Serif Display, serif",
                  paddingTop: "1rem",
                }}
              >
                Selenium
              </Card.Text>
            </Card>
          </Col>
        </Container>
      </Row>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          fontSize: "30px",
          fontFamily: "DM Serif Display, serif",
        }}
      >
        <a href="https://gitlab.com/MaxSomarriba/cs373-group-18.git">
          Click here for GitLab Repository
        </a>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          fontSize: "30px",
          fontFamily: "DM Serif Display, serif",
        }}
      >
        <a href="https://documenter.getpostman.com/view/33487675/2sA35MyeBz">
          Click here for Postman API
        </a>
      </div>

      <hr style={{ margin: "20px 0", width: "50%" }} /> {/* Visible break */}

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          fontSize: "30px",
          fontFamily: "DM Serif Display, serif",
          padding: "10px", // Add some padding to improve readability
        }}
      >
        <strong style={{ fontSize: "36px" }}>Data Sources</strong>
        <ul style={{ textAlign: "left" }}>
          <li><a href="https://data.ny.gov">https://data.ny.gov</a></li>
          <li><a href="https://data.cityofnewyork.us">https://data.cityofnewyork.us</a></li>
          <li><a href="https://www.google.com/maps">https://www.google.com/maps</a></li>
        </ul>
      </div>

    </div>
  );
}
