import React from "react";
import { Link } from "react-router-dom";
import background from "../../pictures/splashPageBackground.jpg";
import Screen from "../../pictures/Pure_black_screen2.jpg";
import "./splash.css";

export default function SplashPage() {
  return (
    <div className="splash" style={{ height: "100vh", overflow: "auto" }}>
      <div
        className="background-image-section"
        style={{
          backgroundImage: `url(${background})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          height: "20vh",
          color: "black",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div
          className="text-overlay"
          style={{
            position: "relative",
            top: "0%",
            left: "0%",
            textAlign: "center",
            zIndex: "2",
          }}
        >
          <h1
            style={{
              color: "#ffffff",
              fontSize: "60px",
              textShadow: "2px 2px 4px rgba(0,0,0,0.5)",
              fontFamily: "DM Serif Display, serif",
            }}
          >
            HomeHope New York
          </h1>
        </div>
      </div>
      <h3
        class="mobileshow"
        style={{
          fontSize: "20px",
          position: "relative",
          textAlign: "center",
          fontFamily: "DM Serif Display, serif",
        }}
      >
        {" "}
        We seek to educate people about the homeless population in New York,
        fostering an environment of empathy and understanding. Our website aims
        to provide the tools and resources to help people find shelters & food
        pantries relative to their area or borough! We seek to educate people
        about the homeless population in New York, fostering an environment of
        empathy and understanding.
        <br></br>
        <br></br>
        <Link
          class="linktext"
          to="/homelessshelters"
          style={{ fontSize: "30px", fontFamily: "DM Serif Display, serif" }}
        >
          Shelters
        </Link>
        <br></br>
        <Link
          class="linktext"
          to="/foodpantriesandkitchens"
          style={{ fontSize: "30px", fontFamily: "DM Serif Display, serif" }}
        >
          Pantries
        </Link>
        <br></br>
        <Link
          class="linktext"
          to="/boroughs"
          style={{ fontSize: "30px", fontFamily: "DM Serif Display, serif" }}
        >
          Boroughs
        </Link>
      </h3>

      <div
        className="image-container"
        style={{ width: "100%", overflow: "hidden", position: "relative" }}
      >
        {/* Text layer */}
        <div
          className="text-overlay"
          style={{
            position: "absolute",
            left: "30%",
            textAlign: "center",
            zIndex: "2",
          }}
        >
          <h1
            class="hideme"
            style={{
              color: "#ffffff",
              fontSize: "35px",
              width: "500px",
              position: "relative",
              marginTop: "60px",
              fontFamily: "DM Serif Display, serif",
            }}
          >
            Welcome! We are HomeHope New York!{" "}
          </h1>
          {/* Button container */}
          <div
            className="hideme button-container"
            style={{
              position: "absolute",
              left: "50%",
              transform: "translate(-50%, 50%)",
              zIndex: "2",
              marginTop: "-85px",
            }}
          >
            <Link
              to="/homelessshelters"
              className="triangle-button"
              style={{
                fontSize: "30px",
                fontFamily: "DM Serif Display, serif",
              }}
            >
              Shelters
            </Link>
            <Link
              to="/foodpantriesandkitchens"
              className="triangle-button"
              style={{
                fontSize: "30px",
                fontFamily: "DM Serif Display, serif",
              }}
            >
              Pantries
            </Link>
            <Link
              to="/boroughs"
              className="triangle-button"
              style={{
                fontSize: "30px",
                fontFamily: "DM Serif Display, serif",
              }}
            >
              Boroughs
            </Link>
            <div
              className="text-overlay"
              style={{
                position: "absolute",
                left: "15%",
                textAlign: "center",
                zIndex: "2",
                marginTop: "240px",
              }}
            >
              <h2
                style={{
                  color: "#ffffff",
                  fontSize: "25px",
                  width: "720px",
                  position: "relative",
                  fontFamily: "DM Serif Display, serif",
                }}
              >
                Our website aims to provide the tools and resources to help
                people find shelters & food pantries relative to their area or
                borough!{" "}
              </h2>
              <div
                className="text-overlay"
                style={{
                  position: "absolute",
                  left: "-30%",
                  textAlign: "center",
                  zIndex: "2",
                  marginTop: "10px",
                }}
              >
                <h3
                  style={{
                    color: "#ffffff",
                    fontSize: "23px",
                    width: "700px",
                    position: "relative",
                    whiteSpace: "nowrap",
                    fontFamily: "DM Serif Display, serif",
                  }}
                >
                  {" "}
                  We seek to educate people about the homeless population in New
                  York, fostering an enviroment of empathy and understanding.{" "}
                </h3>
              </div>
            </div>
          </div>
        </div>

        {/* Image */}
        <img
          src={Screen}
          alt="Screen"
          style={{ width: "100%", height: "80vh", zIndex: "1" }}
        />
      </div>
    </div>
  );
}
