import { React, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Box,
  Center,
  Grid,
  Text,
  Image,
  Heading,
  Link,
  List,
  ListItem,
} from "@chakra-ui/react";
import { getGoogleMapsUrl } from "../services/googleMaps";
import InstanceAttributeCard from "../components/cards/InstanceAttributeCard";
import useAPI from "../../hooks/useAPI.js";
import boroughDataBackup from "../../hooks/boroughData.json";
import nycFoodPantriesDataBackup from "../../hooks/nycFoodPantries.json";
import nycShelterDataBackup from "../../hooks/nycShelterData.json";

const ShelterInstance = () => {
  const { id } = useParams();
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  const {
    data: boroughsRes,
    error: boroughsError,
    loading: boroughsLoading,
  } = useAPI("/Boroughs", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: pantriesRes,
    error: pantriesError,
    loading: pantriesLoading,
  } = useAPI("/Pantries", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: shelterRes,
    error: shelterError,
    loading: shelterLoading,
  } = useAPI(`/Shelters/${id}`);

  // Combine loading states
  const loading = boroughsLoading || pantriesLoading || shelterLoading;

  if (loading) {
    return <div>Loading...</div>;
  }

  const pantries = pantriesRes
    ? pantriesRes?.results
    : nycFoodPantriesDataBackup;
  const boroughs = boroughsRes ? boroughsRes?.results : boroughDataBackup;
  const shelter = shelterRes
    ? shelterRes
    : nycShelterDataBackup.find((s) => s.id === parseInt(id));
  const url = getGoogleMapsUrl(shelter.name);

  return (
    <>
      {
        <Box p={8} mx="auto" maxWidth="container.xl">
          <Center flexDirection="column" mb={6}>
            <Image
              src={imageErr ? alternateImage : shelter.image_url}
              alt={shelter.homebase_office}
              objectFit="contain"
              width="400px"
              height="250px"
              mb={4}
              onError={() => setImageErr(true)}
            />
          </Center>

          <Grid templateColumns="2fr 1fr" gap={8} mb={12}>
            <Box>
              {/* provide as title */}
              <Heading as="h2" size="xl" color="Orange" mb={4}>
                {shelter.name}
              </Heading>
              {/* service type as subtitle */}
              <Text mt={4} fontSize="x-large" mb={6}>
                {shelter.description}
              </Text>
              <Text mt={4} fontSize="x-large">
                {shelter.program_type}
              </Text>

              <InstanceAttributeCard {...shelter} />

              {/* links to other instances */}
              <Grid templateColumns="repeat(2, 1fr)" gap={6} mt={12}>
                {pantries && pantries.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Pantries in {shelter.borough}
                    </Heading>
                    <List spacing={2}>
                      {pantries
                        .filter((pantry) =>
                          pantry.borough
                            .toLowerCase()
                            .includes(shelter.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((pantry) => (
                          <ListItem key={pantry.id}>
                            <Link
                              href={`/foodpantriesandkitchens/${pantry.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              {pantry.name}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}

                {boroughs && boroughs.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Districts in {shelter.borough}
                    </Heading>
                    <List spacing={2}>
                      {boroughs
                        .filter((boroughs) =>
                          boroughs.borough
                            .toLowerCase()
                            .includes(shelter.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((borough) => (
                          <ListItem key={borough.id}>
                            <Link
                              href={`/boroughs/${borough.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              District {borough.community_district_num}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}
              </Grid>
              <iframe
                width="auto"
                height="380"
                frameborder="0"
                styles="border:0;overflow:auto;"
                referrerpolicy="no-referrer-when-downgrade"
                src={url}
                allowfullscreen
              ></iframe>
              <a href="https://www.coalitionforthehomeless.org/donate/">
                <button
                  style={{
                    backgroundColor: "Orange",
                    color: "white",
                    padding: "10px 20px",
                    border: "none",
                    borderRadius: "5px",
                    fontWeight: "bold",
                    cursor: "pointer",
                    fontFamily: "DM Serif Display, serif",
                    margin: "20px",
                  }}
                >
                  Link to Donate!
                </button>
              </a>
            </Box>
          </Grid>
        </Box>
      }
    </>
  );
};

export default ShelterInstance;
