import { React, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Box,
  Center,
  Grid,
  GridItem,
  Text,
  Image,
  Heading,
  Link,
  List,
  ListIcon,
  ListItem,
} from "@chakra-ui/react";
import { getGoogleMapsUrl } from "../services/googleMaps";
import InstanceAttributeCard from "../components/cards/InstanceAttributeCard";
import useAPI from "../../hooks/useAPI";
import boroughDataBackup from "../../hooks/boroughData.json";
import nycFoodPantriesDataBackup from "../../hooks/nycFoodPantries.json";
import nycShelterDataBackup from "../../hooks/nycShelterData.json";

const PantryInstance = () => {
  const { id } = useParams();
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  const {
    data: sheltersRes,
    error: sheltersError,
    loading: sheltersLoading,
  } = useAPI("/Shelters", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: boroughsRes,
    error: boroughsError,
    loading: boroughsLoading,
  } = useAPI("/Boroughs", {
    params: {
      limit: 1000,
    },
  });
  const {
    data: pantryRes,
    error: pantryError,
    loading: pantryLoading,
  } = useAPI(`/Pantries/${id}`);

  // Combine loading states
  const loading = boroughsLoading || pantryLoading || sheltersLoading;

  if (loading) {
    return <div>Loading...</div>;
  }

  const shelters = sheltersRes ? sheltersRes?.results : nycShelterDataBackup;
  const boroughs = boroughsRes ? boroughsRes?.results : boroughDataBackup;
  const pantry = pantryRes
    ? pantryRes
    : nycFoodPantriesDataBackup.find((p) => p.id === parseInt(id));
  const url = getGoogleMapsUrl(pantry.name);

  return (
    <>
      {
        <Box p={8} mx="auto" maxWidth="container.xl">
          <Center flexDirection="column" mb={6}>
            <Image
              src={imageErr ? alternateImage : pantry.image_url}
              alt={pantry.name}
              objectFit="contain"
              width="400px"
              height="250px"
              mb={4}
              onError={() => setImageErr(true)}
            />
          </Center>

          <Grid templateColumns="2fr 1fr" gap={8} mb={12}>
            <Box>
              {/* provide as title */}
              <Heading as="h2" size="xl" color="Orange" mb={4}>
                {pantry.name}
              </Heading>
              {/* program type as subtitle */}
              <Text mt={4} fontSize="x-large">
                {pantry.program_type}
              </Text>

              {/* links to other instances */}
              <Grid templateColumns="repeat(2, 1fr)" gap={6} mt={12}>
                {shelters && shelters.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Shelters in {pantry.borough}
                    </Heading>
                    <List spacing={2}>
                      {shelters
                        .filter((shelter) =>
                          shelter.borough
                            .toLowerCase()
                            .includes(pantry.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((shelter) => (
                          <ListItem key={shelter.id}>
                            <Link
                              href={`/homelessshelters/${shelter.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              {shelter.name}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}

                {boroughs && boroughs.length > 0 && (
                  <Box>
                    <Heading as="h4" size="md" mb={4}>
                      Districts in {pantry.borough}
                    </Heading>
                    <List spacing={2}>
                      {boroughs
                        .filter((boroughs) =>
                          boroughs.borough
                            .toLowerCase()
                            .includes(pantry.borough.toLowerCase())
                        )
                        .slice(0, 5)
                        .map((borough) => (
                          <ListItem key={borough.id}>
                            <Link
                              href={`/boroughs/${borough.id}`}
                              color="Orange.500"
                              fontSize="lg"
                            >
                              District {borough.community_district_num}
                            </Link>
                          </ListItem>
                        ))}
                    </List>
                  </Box>
                )}
              </Grid>
              <InstanceAttributeCard {...pantry} />
              <iframe
                width="auto"
                height="380"
                frameborder="0"
                styles="border:0;overflow:auto;"
                referrerpolicy="no-referrer-when-downgrade"
                src={url}
                allowfullscreen
              ></iframe>
              <a href="https://www.coalitionforthehomeless.org/donate/">
                <button
                  style={{
                    backgroundColor: "Orange",
                    color: "white",
                    padding: "10px 20px",
                    border: "none",
                    borderRadius: "5px",
                    fontWeight: "bold",
                    cursor: "pointer",
                    fontFamily: "DM Serif Display, serif",
                    margin: "20px",
                  }}
                >
                  Link to Donate!
                </button>
              </a>
            </Box>
          </Grid>
        </Box>
      }
    </>
  );
};

export default PantryInstance;
