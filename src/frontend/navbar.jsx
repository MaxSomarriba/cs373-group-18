import React from "react";
import "./navbar.css";
import logo from "../pictures/LogoNew.jpg";

function Navbar() {
  return (
    <nav className="navbar">
      <div className="logoContainer">
        <img src={logo} style={{ height: "40px" }} alt="Logo" />
        <span
          style={{ color: "#ffffff", fontFamily: "DM Serif Display, serif" }}
        >
          {" "}
          HomeHope New York
        </span>
      </div>

      <ul className="nav-list">
        <li className="nav-item">
          <a href="/" className="nav-link">
            Home
          </a>
        </li>
        <li className="nav-item">
          <a href="/about" className="nav-link">
            About
          </a>
        </li>
        <li className="nav-item">
          <a href="/homelessshelters" className="nav-link">
            Homeless Shelters
          </a>
        </li>
        <li className="nav-item">
          <a href="/foodpantriesandkitchens" className="nav-link">
            Food Pantries & Kitchens
          </a>
        </li>
        <li className="nav-item">
          <a href="/boroughs" className="nav-link">
            Boroughs & Community Districts
          </a>
        </li>
        <li className="nav-item">
          <a href="/visualizations" className="nav-link">
            Visualizations
          </a>
        </li>
        <li className="nav-item">
          <a href="/search" className="nav-link">
            Search
          </a>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
