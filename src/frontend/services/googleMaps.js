export const getGoogleMapsUrl = (query) => {
  return `https://www.google.com/maps/embed/v1/place?key=${
    process.env.REACT_APP_GOOGLE_MAPS_API_KEY
  }&q=${query ? query.replace(/\s+/g, "+") : "New+York+City"}`;
};
