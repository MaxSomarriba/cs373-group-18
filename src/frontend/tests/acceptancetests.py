import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import time
url = 'https://www.homehopeny.site/'
class SeleniumAcceptanceTests(unittest.TestCase):
    def setUp(self) -> None:
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-gpu")
        options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=options)
        self.driver.implicitly_wait(10)
        self.driver.get(url)
        return super().setUp()
    
    def tearDown(self) -> None:
        self.driver.quit()
        return super().tearDown()
    
    # tests 1-4: testing the nav bar and seeing if each page is working and clickable
    def test_1(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'About')
        button.click()
        self.assertEqual(self.driver.current_url, url + "about")

    def test_2(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Homeless Shelters')
        button.click()
        self.assertEqual(self.driver.current_url, url + "homelessshelters")

    def test_3(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Food Pantries & Kitchens')
        button.click()
        self.assertEqual(self.driver.current_url, url + "foodpantriesandkitchens")

    def test_4(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Boroughs & Community Districts')
        button.click()
        self.assertEqual(self.driver.current_url, url + "boroughs")

    # tests 5-7: Checking for important keywords on pages, specifically headers
    def test_5(self):
        self.driver.get(url)
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'HomeHope New York')]")
        self.assertEqual(text.text, "HomeHope New York")
    
    def test_6(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'About')
        button.click()
        text = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'About Home Hope')]")
        self.assertEqual(text.text, "About Home Hope")

    def test_7(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Homeless Shelters')
        button.click()
        text = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'Shelters')]")
        self.assertEqual(text.text, "Shelters")


    # tests 8-10: Checking for dynamic counts
    def test_8(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Food Pantries & Kitchens')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Total Results: ')]")
        self.assertEqual(text.text, "Total Results: 250")

    def test_9(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Boroughs & Community Districts')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Total Results: ')]")
        self.assertEqual(text.text, "Total Results: 59")
    
    def test_10(self):
        self.driver.get(url)
        button = self.driver.find_element(By.LINK_TEXT, 'Homeless Shelters')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Total Results: ')]")
        self.assertEqual(text.text, "Total Results: 125")

if __name__ == "__main__":
    unittest.main()