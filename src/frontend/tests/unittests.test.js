import React from "react";
import { render, screen, cleanup, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { ChakraProvider } from "@chakra-ui/react";
import { BrowserRouter as Router } from "react-router-dom"; // Import BrowserRouter
import Splash from "../pages/splash.jsx";
import Navbar from "../navbar.jsx";
import Cities from "../pages/cities.jsx";
import About from "../pages/about.jsx";
import Food from "../pages/food.jsx";
import Shelters from "../pages/shelters.jsx";

// Cleaning up after each test
afterEach(cleanup);

describe("Splash Page component", () => {
  // Test 1: Ensuring the Landing component renders without any errors.
  test("1. Render test for Splash Page component", () => {
    <Router>
      <Splash />;
    </Router>;
  });

  // Test 2: Verifying that the Splash page component contains the specific text "HomeHope New York".
  test('2. Display the "HomeHope New York" text', () => {
    render(
      <Router>
        <Splash />;
      </Router>
    );
    expect(screen.getByText("HomeHope New York")).toBeInTheDocument();
  });

  // Test 3: Verifying that the Splash page component contains the specific text "HomeHope New York".
  test('3. Check for the presence of a heading, which should be the "HomeHope New York text', () => {
    render(
      <Router>
        <Splash />;
      </Router>
    );
    // Use queryAllByText to get all elements containing the text
    const headings = screen.queryAllByText(/HomeHope New York/i);

    // Check if at least one heading exists
    expect(headings.length).toBeGreaterThan(0);
  });

  // Test 4: Verifying that the Splash component displays at least our logo.
  test("4. Ensure an image is displayed on the Landing page", () => {
    render(
      <Router>
        <Splash />;
      </Router>
    );
    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});

describe("NavBar component", () => {
  // Check that all elements are present
  test("5. Check that all elements in the NavBar are present", () => {
    render(<Navbar />);
    expect(screen.getByText("HomeHope New York")).toBeInTheDocument();
    expect(screen.getByText("About")).toBeInTheDocument();
    expect(screen.getByText("Homeless Shelters")).toBeInTheDocument();
    expect(screen.getByText("Food Pantries & Kitchens")).toBeInTheDocument();
    expect(
      screen.getByText("Boroughs & Community Districts")
    ).toBeInTheDocument();
  });
});

describe("Boroughs & Districts component", () => {
  // Test 6: Ensuring the Boroughs & Districts component renders without any errors.
  test("6. Render test for Boroughs & Districts component", async () => {
    render(
      <ChakraProvider>
        <Cities />
      </ChakraProvider>
    );
  });

  // Test 7: Verifying that the Boroughs & Community Districts page component contains the specific text "HomeHope New York".
  test('7. Display the "Boroughs & Community Districts" text', async () => {
    render(
      <ChakraProvider>
        <Cities />
      </ChakraProvider>
    );

    await waitFor(() =>
      expect(
        screen.getByText("Boroughs & Community Districts")
      ).toBeInTheDocument()
    );
  });
});

describe("About page", () => {
  // Test 8: Ensuring the About Page component renders without any errors.
  test("8. Render test for About Page component", () => {
    render(
      <ChakraProvider>
        <About />
      </ChakraProvider>
    );
  });
});
describe("Food Pantries and Kitchens page", () => {
  // Test 9: Ensuring the Food Pantries and Kitchens Page component renders without any errors.
  test("9. Render test for Food Pantries and Kitchens Page component", async () => {
    render(
      <ChakraProvider>
        <Food />
      </ChakraProvider>
    );
  });
});

describe("Homeless Shelters page", () => {
  // Test 10: Ensuring the Homeless Shelters Page component renders without any errors.
  test("10. Render test for Homeless Shelters Page component", async () => {
    render(
      <ChakraProvider>
        <Shelters />
      </ChakraProvider>
    );
  });
});
