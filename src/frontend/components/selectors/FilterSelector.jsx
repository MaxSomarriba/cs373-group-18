import { Menu, MenuButton, Button, MenuList, MenuItem } from "@chakra-ui/react";
import { BsChevronDown } from "react-icons/bs";

const FilterSelector = ({ filterName, filterOptions, selectedFilter, onSelect }) => {
    return (
        <Menu>
            <MenuButton as={Button} rightIcon={<BsChevronDown />}>
                {selectedFilter
                    ? filterOptions.filter((option) => option.id === selectedFilter)[0].label
                    : filterName}
            </MenuButton>
            <MenuList maxHeight="400px" overflowY="auto">
                <MenuItem key={0} onClick={() => onSelect(null)}>
                    {filterName}
                </MenuItem>
                {filterOptions.map((option) => (
                    <MenuItem key={option.id} onClick={() => onSelect(option.id)}>
                        {option.label}
                    </MenuItem>
                ))}
            </MenuList>
        </Menu>
    );
};

export default FilterSelector;
