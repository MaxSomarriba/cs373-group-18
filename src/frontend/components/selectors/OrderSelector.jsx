import { Menu, MenuButton, Button, MenuList, MenuItem } from "@chakra-ui/react";
import { BsChevronDown } from "react-icons/bs";

const OrderSelector = ({selectedOrder, onSelect}) => {
  const sortOrders = [
    { value: "asc", label: "Ascending" },
    { value: "desc", label: "Descending" },
  ];

  return (
    <Menu>
      <MenuButton as={Button} rightIcon={<BsChevronDown />} fontSize="18px" fontFamily='DM Serif Display, serif'>
        {sortOrders.find(order => order.value === selectedOrder)?.label || "Order"}
      </MenuButton>
      <MenuList>
        {sortOrders.map((order) => (
          <MenuItem key={order.value} onClick={() => onSelect(order.value)}>{order.label}</MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export default OrderSelector;