import React from "react";
import Harini1557 from "../../../pictures/Harini.jpg";
import MaxSomarriba from "../../../pictures/MaxPicture.jpg";
import larissafranco from "../../../pictures/LarissaPicture.jpg";
import nathanjacob from "../../../pictures/IMG_3804.jpg";
import dillonluong from "../../../pictures/ProfessionalHeadshot.jpg";

import { Card } from "react-bootstrap";

const usernameToImage = {
  Harini1557,
  MaxSomarriba,
  larissafranco,
  nathanjacob,
  dillonluong,
};

function Team({ stat }) {
  const imageSrc = usernameToImage[stat.username];

  return (
    <div className="team-container">
      <Card.Img
        style={{ justifyContent: "center" }}
        variant="top"
        src={imageSrc}
        width={285}
        fit="true"
      />
    </div>
  );
}

export default Team;
