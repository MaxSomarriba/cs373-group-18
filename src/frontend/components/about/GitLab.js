import { Card } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import axios from "axios";
import TeamImage from "./TeamImages"; // Import the Image component

const GitLabStats = () => {
  const [ownerStats, setOwnerStats] = useState([]);
  const personalAccessToken = "glpat-p8tefWhwSYXz9qDsaKXe";
  const projectId = "54616025";

  useEffect(() => {
    const predefinedUsers = [
      "Harini1557",
      "MaxSomarriba",
      "larissafranco",
      "nathanjacob",
      "dillonluong",
    ];
    const fetchData = async () => {
      try {
        const ownerStatsData = await Promise.all(
          predefinedUsers.map(async (username) => {
            let author = username;

            if (username === "MaxSomarriba") {
              author = "Max";
            }
            if (username === "Harini1557") {
              author = "Harini Arivalagan";
            }
            if (username === "larissafranco") {
              author = "larissafranco";
            }
            if (username === "nathanjacob") {
              author = "Nathan Jacob";
            }
            if (username === "dillonluong") {
              author = "Dillon Luong";
            }

            const commitsUrl = `https://gitlab.com/api/v4/projects/${projectId}/repository/commits?author=${author}&all=true&per_page=100`;
            const commitsResponse = await axios.get(commitsUrl, {
              headers: { "PRIVATE-TOKEN": personalAccessToken },
            });
            let commitsCount = commitsResponse.data.length;
            const issuesUrl = `https://gitlab.com/api/v4/projects/${projectId}/issues?author_username=${username}`;
            const issuesResponse = await axios.get(issuesUrl, {
              headers: { "PRIVATE-TOKEN": personalAccessToken },
            });
            const issuesCount = issuesResponse.data.length;

            return { username, commitsCount, issuesCount };
          })
        );

        setOwnerStats(ownerStatsData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []); // Include predefinedUsers in the dependency array

  const usersData = [
    {
      name: "Harini Arivalagan",
      username: "Harini1557",
      bio: "I am a third year CS major at UT Austin. In my free time, I enjoy crocheting and watching anime.",
      role: "AWS & Frontend",
      tests: 0,
    },
    {
      name: "Max Somarriba",
      username: "MaxSomarriba",
      bio: "I'm a third-year CS major at UT Austin. I enjoy jiu-jitsu and watching tv shows.",
      role: "Frontend and Database",
      tests: 0,
    },
    {
      name: "Larissa Franco",
      username: "larissafranco",
      bio: "I'm a fourth year CS major at UT Austin. I love going to concerts, playing soccer, and traveling:)",
      role: "Frontend",
      tests: 0,
    },
    {
      name: "Nathan Jacob",
      username: "nathanjacob",
      bio: "I am a second year CS major at UT Austin. I love hiking, camping, trekking, and anything else outdoors",
      role: "Backend",
      tests: 0,
    },
    {
      name: "Dillon Luong",
      username: "dillonluong",
      bio: "I am a third year CS major at UT Austin. I'm on the fencing club and I write for SPARK magazine",
      role: "Backend",
      tests: 0,
    },
  ];

  return (
    <div>
      <div>
        {ownerStats.map((stat) => {
          const user = usersData.find(
            (user) => user.username === stat.username
          );
          return (
            <div key={stat.username} style={{ display: "inline-grid" }}>
              <Card style={{ width: "20rem", border: "2px solid #000000" }}>
                <Card.Body>
                  <div style={{ display: "flex", justifyContent: "center" }}>
                    <TeamImage stat={user} />
                  </div>
                  <Card.Title>{user.name}</Card.Title>
                  <Card.Header> Role : {user.role} </Card.Header>
                  <Card.Text>{user.bio}</Card.Text>
                  <Card.Footer>GitLab ID: {user.username}</Card.Footer>
                  <Card.Footer>Commits: {stat.commitsCount}</Card.Footer>
                  <Card.Footer>Issues: {stat.issuesCount}</Card.Footer>
                  <Card.Footer>Tests: {user.tests}</Card.Footer>
                </Card.Body>
              </Card>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default GitLabStats;
