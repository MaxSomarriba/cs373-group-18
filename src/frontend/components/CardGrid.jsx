import { useEffect } from "react";
import { SimpleGrid, Text, Center } from "@chakra-ui/react";
import CardSkeleton from "./cards/CardSkeleton";
import Paginator from "./Paginator";

const CardGrid = ({
  CardComponent,
  modelData,
  totalResults,
  loading,
  currentPage,
  onPageChange,
  cardLimit,
  searchValue = "",
}) => {
  useEffect(() => {
    if (loading) {
      window.scrollTo(0, 0);
    }
  }, [loading]);

  const skeletons = [...Array(9).keys()];

  return (
    <>
      {loading && (
        <SimpleGrid columns={{ sm: 1, md: 2, lg: 3, xl: 3 }} spacing={6}>
          {skeletons.map((id) => (
            <CardSkeleton key={id} />
          ))}
        </SimpleGrid>
      )}
      {!loading && modelData && modelData.length > 0 && (
        <>
          <SimpleGrid columns={{ sm: 1, md: 2, lg: 3, xl: 3 }} spacing={6}>
            {modelData.map((model) => (
              <CardComponent key={model.id} search={searchValue} {...model} />
            ))}
          </SimpleGrid>
          <Paginator
            currentPage={currentPage}
            totalPages={Math.ceil(totalResults / cardLimit)}
            onPageChange={onPageChange}
          />
          <Center mb={4}>
            <Text fontSize="18px">
              Page {Math.ceil(modelData.length / 24)} of{" "}
              {Math.ceil(totalResults / 24)}
            </Text>
          </Center>
        </>
      )}
      {!loading && modelData && modelData.length === 0 && (
        <Center mt="1em">
          <Text fontSize="lg">No results</Text>
        </Center>
      )}
    </>
  );
};

export default CardGrid;
