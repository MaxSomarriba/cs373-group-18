import React from "react";
import { Icon, Flex, Button } from "@chakra-ui/react";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";

export default function Paginator({ currentPage, totalPages, onPageChange }) {
  const maxDisplayedPages = 10;

  let startPage, endPage;
  if (totalPages <= maxDisplayedPages) {
    // <max, no need for anything fancy
    startPage = 1;
    endPage = totalPages;
  } else {
    if (currentPage <= 6) {
      // early
      startPage = 1;
      endPage = maxDisplayedPages;
    } else if (currentPage + 4 >= totalPages) {
      // nearing end
      startPage = totalPages - 9;
      endPage = totalPages;
    } else {
      // somewhere middle
      startPage = currentPage - 5;
      endPage = currentPage + 5;
    }
  }

  const PagButton = (props) => {
    return (
      <Button
        mx={1}
        width={5}
        rounded="md"
        isDisabled={props.disabled}
        variant={props.active ? "solid" : "ghost"}
        onClick={props.onClick}
        colorScheme="teal"
        fontSize="18px"
      >
        {props.children}
      </Button>
    );
  };

  return (
    <Flex
      bg="white"
      p={50}
      w="full"
      alignItems="center"
      justifyContent="center"
    >
      <Flex>
        <PagButton
          disabled={currentPage === 1}
          onClick={() => onPageChange(currentPage - 1)}
        >
          <Icon as={IoIosArrowBack} color="teal.600" boxSize={5} />
        </PagButton>
        {Array.from({ length: endPage - startPage + 1 }).map((_, index) => (
          <PagButton
            key={index + startPage}
            active={index + startPage === currentPage}
            onClick={() => onPageChange(index + startPage)}
          >
            {index + startPage}
          </PagButton>
        ))}
        <PagButton
          disabled={currentPage === totalPages}
          onClick={() => onPageChange(currentPage + 1)}
        >
          <Icon as={IoIosArrowForward} color="teal.600" boxSize={5} />
        </PagButton>
      </Flex>
    </Flex>
  );
}
