import { useRef } from "react";
import { Input, InputGroup, InputLeftElement, Box } from "@chakra-ui/react";
import { IoSearchOutline } from "react-icons/io5";
import { BsAlignMiddle } from "react-icons/bs";
import { px } from "framer-motion";

const SearchBar = ({ initialValue = "", onSearch }) => {
  const ref = useRef(null);
  return (
    <Box flex={1}>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          if (ref.current) onSearch(ref.current.value);
        }}
      >
        <InputGroup size="20px">
          <InputLeftElement
            pointerEvents="none"
            paddingTop={2.5}
            paddingLeft={1}
          >
            <IoSearchOutline color="gray.300" />
          </InputLeftElement>
          <Input
            ref={ref}
            defaultValue={initialValue}
            variant="outline"
            placeholder="Search"
            width="full"
            focusBorderColor="teal.500"
            bgColor="white"
            fontSize="18px"
            pl="2.5rem"
          />
        </InputGroup>
      </form>
    </Box>
  );
};

export default SearchBar;
