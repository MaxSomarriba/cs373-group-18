import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import useAPI from "../../../hooks/useAPI";
import nycShelterDataBackup from "../../../hooks/nycShelterData.json";
import { border } from "@chakra-ui/react";

const ShelterPieChart = () => {
  const chartRef = useRef();
  const legendRef = useRef();

  const {
    data: sheltersManhattan,
    error: sheltersError,
    loading: sheltersLoading,
  } = useAPI("/Shelters", {
    params: {
      borough: "Manhattan",
      limit: 1000,
    },
  });
  const {
    data: sheltersBronx,
    error: sheltersErrorBronx,
    loading: sheltersLoadingBronx,
  } = useAPI("/Shelters", {
    params: {
      borough: "Bronx",
      limit: 1000,
    },
  });
  const {
    data: sheltersBrooklyn,
    error: sheltersErrorBrooklyn,
    loading: sheltersLoadingBrooklyn,
  } = useAPI("/Shelters", {
    params: {
      borough: "Brooklyn",
      limit: 1000,
    },
  });
  const {
    data: sheltersQueens,
    error: sheltersErrorQueens,
    loading: sheltersLoadingQueens,
  } = useAPI("/Shelters", {
    params: {
      borough: "Queens",
      limit: 1000,
    },
  });
  const {
    data: sheltersStatenIsland,
    error: sheltersErrorStatenIsland,
    loading: sheltersLoadingStatenIsland,
  } = useAPI("/Shelters", {
    params: {
      borough: "Staten Island",
      limit: 1000,
    },
  });

  const loading =
    sheltersLoading ||
    sheltersLoadingBronx ||
    sheltersLoadingBrooklyn ||
    sheltersLoadingQueens ||
    sheltersLoadingStatenIsland;

  useEffect(() => {
    if (
      !loading &&
      chartRef.current &&
      sheltersManhattan &&
      sheltersBronx &&
      sheltersBrooklyn &&
      sheltersQueens &&
      sheltersStatenIsland
    ) {
      const data = [
        { label: "Manhattan", count: sheltersManhattan?.count || 0 },
        { label: "Bronx", count: sheltersBronx?.count || 0 },
        { label: "Brooklyn", count: sheltersBrooklyn?.count || 0 },
        { label: "Queens", count: sheltersQueens?.count || 0 },
        { label: "Staten Island", count: sheltersStatenIsland?.count || 0 },
      ];

      const width = 400;
      const height = 400;
      const radius = Math.min(width, height) / 2;
      d3.select(chartRef.current).selectAll("svg").remove();

      const svg = d3
        .select(chartRef.current)
        .append("svg")
        .attr("width", width)
        .attr("height", height);

      const color = d3
        .scaleOrdinal()
        .domain(data.map((d) => d.label))
        .range(d3.schemeCategory10);

      const arc = d3.arc().innerRadius(0).outerRadius(radius);

      const pie = d3.pie().value((d) => d.count);

      const arcs = svg
        .selectAll("arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc")
        .attr("transform", `translate(${width / 2},${height / 2})`);

      arcs
        .append("path")
        .attr("d", arc)
        .attr("fill", (d) => color(d.data.label))
        .attr("stroke", "#fff")
        .style("stroke-width", "2px");

      const legend = d3.select(legendRef.current);

      const legendItems = legend
        .selectAll(".legend-item")
        .data(data)
        .enter()
        .append("div")
        .attr("class", "legend-item")
        .style("display", "flex")
        .style("align-items", "center")
        .style("margin-bottom", "5px");

      legendItems
        .append("div")
        .style("width", "10px")
        .style("height", "10px")
        .style("background-color", (d) => color(d.label))
        .style("margin-right", "5px");

      legendItems
        .append("span")
        .text((d) => `${d.label}`)
        .style("font-size", "12px");

      svg
        .append("g")
        .attr("transform", `translate(${width / 2},${height + 20})`)
        .call(d3.axisBottom().scale(color));
    }
  }, [
    chartRef,
    loading,
    sheltersManhattan,
    sheltersBronx,
    sheltersBrooklyn,
    sheltersQueens,
    sheltersStatenIsland,
  ]);

  return (
    <div>
      <div ref={chartRef}></div>
      <div ref={legendRef}></div>
    </div>
  );
};

export default ShelterPieChart;
