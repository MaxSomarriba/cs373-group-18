import React, { useState, useEffect, useRef } from "react";
import * as d3 from "d3";
import axios from "axios";

const PieChart = () => {
  const chartRef = useRef();

  const url = "https://api.autoimmunediseasecenter.me/disease";
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(url);
        setData(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (chartRef.current && data.length) {
      drawChart();
    }
  }, [data]);

  const countRarity = () => {
    const rarityCounts = {};
    data.forEach((disease) => {
      const rarity = disease.attribute_rarity;
      rarityCounts[rarity] = (rarityCounts[rarity] || 0) + 1;
    });
    return Object.entries(rarityCounts);
  };

  const drawChart = () => {
    const margin = { top: 50, right: 50, bottom: 50, left: 50 };
    const width = 400;
    const height = 400;
    const radius = Math.min(width, height) / 2 - 10;

    // Clear previous contents (if any)
    d3.select(chartRef.current).selectAll("*").remove();

    const svg = d3
      .select(chartRef.current)
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr(
        "transform",
        `translate(${margin.left + width / 2},${margin.top + height / 2})`
      );

    const colorScale = d3.scaleOrdinal(d3.schemeCategory10);

    const pie = d3.pie().value((d) => d[1]);
    const data_ready = pie(countRarity());

    const arc = d3.arc().innerRadius(0).outerRadius(radius);

    svg
      .selectAll("slices")
      .data(data_ready)
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("fill", (d) => colorScale(d.data[0]))
      .attr("stroke", "white")
      .style("stroke-width", "2px");

    // Add legend
    const legend = svg
      .selectAll(".legend")
      .data(data_ready)
      .enter()
      .append("g")
      .attr("class", "legend")
      .attr(
        "transform",
        (d, i) => `translate(-${width / 2 - 200},${i * 20 - height / 2})`
      );

    legend
      .append("rect")
      .attr("x", -220)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", (d) => colorScale(d.data[0]));

    legend
      .append("text")
      .attr("x", -198)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .style("font-weight", "bold")
      .style("font-size", "8px")
      .text((d) => `${d.data[0]} (${d.data[1]})`);
  };

  return <svg ref={chartRef}></svg>;
};

export default PieChart;
