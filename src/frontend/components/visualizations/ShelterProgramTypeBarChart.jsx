import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import useAPI from "../../../hooks/useAPI";
import { border } from "@chakra-ui/react";

const ShelterProgramTypeBarChart = () => {
  const chartRef = useRef();

  const {
    data: sheltersFamily,
    error: sheltersErrorFamily,
    loading: sheltersLoadingFamily,
  } = useAPI("/Shelters", {
    params: {
      program_type: "Family",
      limit: 1000,
    },
  });
  const {
    data: sheltersYouth,
    error: sheltersErrorYouth,
    loading: sheltersLoadingYouth,
  } = useAPI("/Shelters", {
    params: {
      program_type: "Youth",
      limit: 1000,
    },
  });
  const {
    data: sheltersAdult,
    error: sheltersErrorAdult,
    loading: sheltersLoadingAdult,
  } = useAPI("/Shelters", {
    params: {
      program_type: "Adult",
      limit: 1000,
    },
  });
  const {
    data: sheltersHomebase,
    error: sheltersErrorHomebase,
    loading: sheltersLoadingHomebase,
  } = useAPI("/Shelters", {
    params: {
      program_type: "Homebase",
      limit: 1000,
    },
  });

  useEffect(() => {
    if (
      chartRef.current &&
      sheltersFamily &&
      sheltersYouth &&
      sheltersAdult &&
      sheltersHomebase
    ) {
      const data = [
        { programType: "Family", count: sheltersFamily?.count || 0 },
        { programType: "Youth", count: sheltersYouth?.count || 0 },
        { programType: "Adult", count: sheltersAdult?.count || 0 },
        { programType: "Homebase", count: sheltersHomebase?.count || 0 },
      ];

      console.log("Processed data:", data);

      const margin = { top: 20, right: 30, bottom: 30, left: 40 };
      const width = 600 - margin.left - margin.right;
      const height = 400 - margin.top - margin.bottom;
      d3.select(chartRef.current).selectAll("svg").remove();
      const svg = d3
        .select(chartRef.current)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      const x = d3
        .scaleBand()
        .domain(data.map((d) => d.programType))
        .range([0, width])
        .padding(0.1);
      const y = d3
        .scaleLinear()
        .domain([0, d3.max(data, (d) => d.count)])
        .nice()
        .range([height, 0]);

      svg
        .append("g")
        .attr("transform", `translate(0,${height})`)
        .call(d3.axisBottom(x));
      svg.append("g").call(d3.axisLeft(y));

      svg
        .selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", (d) => x(d.programType))
        .attr("y", (d) => y(d.count))
        .attr("width", x.bandwidth())
        .attr("height", (d) => height - y(d.count))
        .attr("fill", "#69b3a2");
    }
  }, [
    chartRef,
    sheltersFamily,
    sheltersYouth,
    sheltersAdult,
    sheltersHomebase,
  ]);

  return <div ref={chartRef}></div>;
};

export default ShelterProgramTypeBarChart;
