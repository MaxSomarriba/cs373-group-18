import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import useAPI from "../../../hooks/useAPI";

const PantriesLicenseTypeChart = () => {
  const chartRef = useRef();
  const legendRef = useRef();

  const { data: unlicensedPantries } = useAPI("/Pantries", {
    params: {
      approval_license_type: "Unlicensed",
      limit: 1000,
    },
  });
  const { data: nycLicensedPantries } = useAPI("/Pantries", {
    params: {
      approval_license_type: "NYC DEPARTMENT OF HEALTH AND MENTAL HYGIENE",
      limit: 1000,
    },
  });
  const { data: officeLicensedPantries } = useAPI("/Pantries", {
    params: {
      approval_license_type: "OFFICE OF CHILDREN AND FAMILY SERVICES",
      limit: 1000,
    },
  });

  useEffect(() => {
    if (
      chartRef.current &&
      unlicensedPantries &&
      nycLicensedPantries &&
      officeLicensedPantries
    ) {
      const data = [
        { label: "Unlicensed", count: unlicensedPantries?.count || 0 },
        { label: "NYC Licensed", count: nycLicensedPantries?.count || 0 },
        { label: "Office Licensed", count: officeLicensedPantries?.count || 0 },
      ];

      d3.select(chartRef.current).selectAll("svg").remove();
      const width = 400;
      const height = 400;
      const radius = Math.min(width, height) / 2;

      const color = d3
        .scaleOrdinal()
        .domain(data.map((d) => d.label))
        .range(d3.schemeCategory10);

      const arc = d3
        .arc()
        .innerRadius(radius - 100)
        .outerRadius(radius - 20);

      const pie = d3.pie().value((d) => d.count);

      const svg = d3
        .select(chartRef.current)
        .append("svg")
        .attr("width", width)
        .attr("height", height);

      const arcs = svg
        .selectAll("arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc")
        .attr("transform", `translate(${width / 2},${height / 2})`);

      arcs
        .append("path")
        .attr("d", arc)
        .attr("fill", (d) => color(d.data.label))
        .attr("stroke", "#fff")
        .style("stroke-width", "2px");

      const legend = d3.select(legendRef.current);

      const legendItems = legend
        .selectAll(".legend-item")
        .data(data)
        .enter()
        .append("div")
        .attr("class", "legend-item")
        .style("display", "flex")
        .style("align-items", "center")
        .style("margin-bottom", "5px");

      legendItems
        .append("div")
        .style("width", "10px")
        .style("height", "10px")
        .style("background-color", (d) => color(d.label))
        .style("margin-right", "5px");

      legendItems
        .append("span")
        .text((d) => `${d.label}`)
        .style("font-size", "12px");

      svg
        .append("g")
        .attr("transform", `translate(${width / 2},${height + 20})`)
        .call(d3.axisBottom().scale(color));
    }
  }, [
    chartRef,
    unlicensedPantries,
    nycLicensedPantries,
    officeLicensedPantries,
  ]);

  return (
    <div>
      <div ref={chartRef}></div>
      <div ref={legendRef}></div>
    </div>
  );
};

export default PantriesLicenseTypeChart;
