import { useState } from "react";
import {
  Box,
  Image,
  Heading,
  Text,
  Button,
  VStack,
  Card,
  Flex,
  Highlight,
} from "@chakra-ui/react";

const formatTime = (timeString) => {
  const [hours, minutes] = timeString.split(":");
  return `${parseInt(hours, 10) % 12 || 12}:${minutes} ${
    parseInt(hours, 10) >= 12 ? "pm" : "am"
  }`;
};

const formatScheduleString = (schedule) => {
  if (schedule.length === 0) {
    return "Not Available";
  }

  const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  return schedule
    .filter((day) => day && day.weekday && day.opens_at && day.closes_at) // Filter out items with missing values
    .map((day) => {
      const weekday = weekdays[parseInt(day.weekday, 10) - 1];
      const opensAt = formatTime(day.opens_at);
      const closesAt = formatTime(day.closes_at);
      return `${weekday}: ${opensAt}-${closesAt}`;
    })
    .join(" | ");
};

const FoodPantriesCard = (props) => {
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  return (
    <Card borderRadius={10} overflow="hidden" height={"auto"} mb={6}>
      <Image
        src={imageErr ? alternateImage : props.image_url}
        alt={props.name}
        w="100%"
        h="200px"
        objectFit="contain"
        onError={() => setImageErr(true)}
      />
      <VStack align="start" spacing={1} p={4} pt={0} flexGrow={1}>
        <Heading fontSize="12px" fontWeight="semibold">
          <Text as="strong" fontFamily="DM Serif Display, serif"></Text>
          <Text as="span" fontFamily="DM Serif Display, serif">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.name ?? "Null"}
            </Highlight>
          </Text>
        </Heading>

        {/* name*/}
        <Box noOfLines={2} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Name:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.name ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Borough:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.borough ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* phone # */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Address:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.address ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* website url */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Site Type:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.site_type ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* program type */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Program Type:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.program_type ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* schedule */}
        <Box noOfLines={2} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Approved License Type:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.approval_license_type ?? "Null"}
            </Highlight>
          </Text>
        </Box>
      </VStack>
      <Flex justify="center" mb={6}>
        <Button
          as="a"
          href={`/foodpantriesandkitchens/${props.id}`}
          colorScheme="orange"
          fontFamily="DM Serif Display, serif"
          fontSize="18px"
        >
          Learn More
        </Button>
      </Flex>
    </Card>
  );
};

export default FoodPantriesCard;
