import { useState } from "react";
import {
  Box,
  Image,
  Heading,
  Text,
  Button,
  VStack,
  Card,
  Flex,
  Highlight,
} from "@chakra-ui/react";

const ShelterCard = (props) => {
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  return (
    <Card borderRadius={10} overflow="hidden" height={"auto"}>
      <Image
        src={imageErr ? alternateImage : props.image_url}
        alt={props.homebase_office}
        w="100%"
        h="200px"
        objectFit="contain"
        onError={() => setImageErr(true)}
      />
      <VStack align="start" spacing={0} p={4} pt={0} flexGrow={1}>
        <Heading fontSize="12px" fontWeight="semibold">
          <Text as="strong" fontFamily="DM Serif Display, serif"></Text>
          <Text as="span" fontFamily="DM Serif Display, serif">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.name ?? "Null"}
            </Highlight>
          </Text>
        </Heading>

        {/* agency/provider */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Provider:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.name ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* borough */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Borough:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.borough ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* address*/}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Address:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.address ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* phone */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Phone:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.phone ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* Type of Service */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Service:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.program_type ?? "Null"}
            </Highlight>
          </Text>
        </Box>
      </VStack>
      <Flex justify="center" mb={4}>
        <Button
          as="a"
          href={`/homelessshelters/${props.id}`}
          colorScheme="orange"
          fontFamily="DM Serif Display, serif"
          fontSize="18px"
        >
          Learn More
        </Button>
      </Flex>
    </Card>
  );
};

export default ShelterCard;
