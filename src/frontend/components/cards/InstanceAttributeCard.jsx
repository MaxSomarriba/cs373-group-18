import React from "react";
import { Box, List, ListItem } from "@chakra-ui/react";

const InstanceAttributeCard = (props) => {
  return (
    <Box>
      {/* list of address, phone, and borough attribute items */}
      <List
        spacing={4}
        p={6}
        bg="gray.100"
        borderRadius="lg"
        boxShadow="sm"
        fontSize="lg"
      >
        {props.name && (
          <ListItem>
            <strong>Name: </strong>
            {props.name}
          </ListItem>
        )}

        {/* attributes for shelters */}
        {props.address && (
          <ListItem>
            <strong>Address: </strong>
            {props.address}
          </ListItem>
        )}
        {props.phone && (
          <ListItem>
            <strong>Phone Number: </strong>
            {props.phone}
          </ListItem>
        )}
        {props.borough && (
          <ListItem>
            <strong>Borough: </strong>
            {props.borough}
          </ListItem>
        )}
        {props.community_district_num && (
          <ListItem>
            <strong>District Number: </strong>
            {props.community_district_num}
          </ListItem>
        )}
        {props.program_type && (
          <ListItem>
            <strong>Program: </strong>
            {props.program_type}
          </ListItem>
        )}
        {props.site_type && (
          <ListItem>
            <strong>Site Type: </strong>
            {props.site_type}
          </ListItem>
        )}
        {props.approval_license_type && (
          <ListItem>
            <strong>License: </strong>
            {props.approval_license_type}
          </ListItem>
        )}
        {/* attributes for pantries */}
        {props.adult_buildings && (
          <ListItem>
            <strong>Adult Buildings: </strong>
            {props.adult_buildings}
          </ListItem>
        )}
        {props.family_buildings && (
          <ListItem>
            <strong>Family Buildings: </strong>
            {props.family_buildings}
          </ListItem>
        )}
        {props.fwc_buildings && (
          <ListItem>
            <strong>FWC Buildings: </strong>
            {props.fwc_buildings}
          </ListItem>
        )}
        {props.cases_count && (
          <ListItem>
            <strong>Cases Count: </strong>
            {props.cases_count}
          </ListItem>
        )}
        {props.individual_count && (
          <ListItem>
            <strong>Individual Count: </strong>
            {props.individual_count}
          </ListItem>
        )}
        {props.individuals && (
          <ListItem>
            <strong>Individuals: </strong>
            {props.individuals}
          </ListItem>
        )}
        {props.community_district_num && (
          <ListItem>
            <strong>Community District: </strong>
            {props.community_district_num}
          </ListItem>
        )}
        {props.case_type && (
          <ListItem>
            <strong>Case Type: </strong>
            {props.cases_count}
          </ListItem>
        )}
        {props.cases && (
          <ListItem>
            <strong>Cases: </strong>
            {props.cases}
          </ListItem>
        )}
      </List>
    </Box>
  );
};

export default InstanceAttributeCard;
