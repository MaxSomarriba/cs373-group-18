import { useState } from "react";
import {
  Box,
  Image,
  Heading,
  Text,
  Button,
  VStack,
  Card,
  Flex,
  Highlight,
} from "@chakra-ui/react";

const CityCard = (props) => {
  const alternateImage = "../../pictures/LogoNew.jpg";
  const [imageErr, setImageErr] = useState(false);

  return (
    <Card borderRadius={10} overflow="hidden" height={"auto"}>
      <Image
        src={imageErr ? alternateImage : props.image_url}
        alt={props.borough}
        w="100%"
        h="200px"
        objectFit="contain"
        onError={() => setImageErr(true)}
      />
      <VStack align="start" spacing={1} p={4} pt={0} flexGrow={1}>
        <Heading fontSize="12px" fontWeight="semibold">
          <Text as="strong" fontFamily="DM Serif Display, serif"></Text>
          <Text as="span" fontFamily="DM Serif Display, serif">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.borough ?? "Null"}
            </Highlight>
          </Text>
        </Heading>

        {/* agency/provider */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Borough:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {props.borough ?? "Null"}
            </Highlight>
          </Text>
        </Box>

        {/* community_district_num */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Community District:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.community_district_num) ?? "Null"}
            </Highlight>
          </Text>
        </Box>

        {/* address*/}
        {/* phone */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Adult Buildings:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.adult_buildings) ?? "Null"}
            </Highlight>
          </Text>
        </Box>

        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Family Buildings:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.family_buildings) ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            FWC Buildings:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.fwc_buildings) ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Individuals:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.individual_count) ?? "Null"}
            </Highlight>
          </Text>
        </Box>
        {/* Type of Service */}
        <Box noOfLines={1} mb={0}>
          <Text
            as="strong"
            fontFamily="DM Serif Display, serif"
            fontSize="18px"
          >
            Cases:{" "}
          </Text>
          <Text as="span" fontFamily="DM Serif Display, serif" fontSize="18px">
            <Highlight
              query={props.search}
              styles={{ px: "0", py: "0", bg: "teal.100" }}
            >
              {String(props.cases_count) ?? "Null"}
            </Highlight>
          </Text>
        </Box>
      </VStack>
      <Flex justify="center" mb={4}>
        <Button
          as="a"
          href={`/boroughs/${props.id}`}
          colorScheme="orange"
          fontFamily="DM Serif Display, serif"
          fontSize="18px"
        >
          Learn More
        </Button>
      </Flex>
    </Card>
  );
};

export default CityCard;
