import React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import "./App.css";
import { useEffect } from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import About from "./frontend/pages/about.jsx";
import Splash from "./frontend/pages/splash.jsx";
import Shelters from "./frontend/pages/shelters.jsx";
import Search from "./frontend/pages/search.jsx";
import Food from "./frontend/pages/food.jsx";
import Cities from "./frontend/pages/cities.jsx";
import ShelterInstance from "./frontend/pages/shelterinstance.jsx";
import FoodInstance from "./frontend/pages/foodinstance.jsx";
import BoroughInstance from "./frontend/pages/citiesinstance.jsx";
import Visualizations from "./frontend/pages/visualizations.jsx";

function App() {
  useEffect(() => {
    document.body.classList.add("loading-page");
    return function cleanup() {
      document.body.classList.remove("landing");
    };
  });

  return (
    <ChakraProvider>
      <Router>
        <Routes>
          <Route path="/about" element={<About />} />
          <Route path="/" element={<Splash />} />

          <Route path="/homelessshelters" element={<Shelters />} />
          <Route path="/homelessshelters/:id" element={<ShelterInstance />} />

          <Route path="/foodpantriesandkitchens" element={<Food />} />
          <Route
            path="/foodpantriesandkitchens/:id"
            element={<FoodInstance />}
          />

          <Route path="/search" element={<Search />} />

          <Route path="/boroughs" element={<Cities />} />
          <Route path="/boroughs/:id" element={<BoroughInstance />} />

          <Route path="/visualizations" element={<Visualizations />} />
        </Routes>
      </Router>
    </ChakraProvider>
  );
}

export default App;
