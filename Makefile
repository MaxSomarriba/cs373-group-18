SHELL	:= bash
BLACK	:= black

	
# start the react app
start:
	cd ./src && npm start

commit:
	git add -A
	@read -p "commit message: " COMMITMSG; \
	git commit -m "$$COMMITMSG"
	git push

# add all files to git
add:
	git add -A
	git status

# pull from gitlab
pull:
	git pull
	git status

install:
	cd ./frontend/src && npm install


