# cs373-group-18

**Website Link**
https://www.homehopeny.site/

**Postman API**
https://documenter.getpostman.com/view/33487675/2sA35MyeBz

**GitLab Pipeline URL**
https://gitlab.com/MaxSomarriba/cs373-group-18/-/pipelines

**GitLab SHA** 
30d606417f04d549af4d7ee6ddfffe0c50f310ae

**Youtube Link**
https://youtu.be/DIVS96wuhmg

**Canvas Group Number**: 
18

**Names of the team members:**
Max Somarriba, Dillon Luong, Harini Arivalagan, Larissa, Nathan Jacob

**Name of the project:** 
HomeHope New York

**The proposed project:** 
The homeless population of the United States has exploded in recent years esspecially in the face of worsening housing costs. HomeHope is a website aimed to be the center for information regarding where people struggling with poverty and homeless can find resources. Our website will contain information about the locations and hours of operations about homeless assistance facilities. Facilities will be connected to the agencies that run them and what city they are located in. 

**URLs of at least three data sources that you will programmatically scrape:**
1. Restful API: https://www.homelessshelterdirectory.org/state/new-york.html (New York Locations and Statistics)
2. https://developers.google.com/maps (Google Maps API)
3. https://scrape-it.cloud/google-images-api (Google Images API) 
4. https://data.ny.gov/
5. https://data.cityofnewyork.us/
6. https://health.data.ny.gov

**Models:**
1. Different facilities for the homeless
2. Food pantries and kitchens
3. Stats on the various boroughs and their respective community districts

**An estimate of the number of instances of each model**
 ~50 boroughs/cds, ~100 pantries, ~200 shelters

**Attributes of Each Model:** 
1. Homeless shelters
    - Alphabetical order (name) 
    - Borough
    - Address
    - Phone
    - Program Type
2. Food Pantries
    - Alphabetical order (name) 
    - Borough
    - Address
    - Site Type
    - Approval License Type
3. Boroughs & Community Districts
    - Borough
    - Community District Number
    - Adult Buildings
    - Family Buildings
    - FWC Buildings
    - Individual Count
    - Cases Count

**Instances of each model must connect to instances of at least two other models:**
Shelters are connected to the borough and districts that belong to it, as well as pantries in the same borough. Pantries are connected to the borough and districts that belong to it, as well as shelters in the same borough. Boroughs/Community Districts are connected to shelters and pantries that belong in that borough.

**Describe two types of media for instances of each model:**
1. Shelters
    - Map location 
    - Images of the building
2. Pantries
    - Map location 
    - Images of the building
3. Boroughs/Community Districts 
    - Map location 
    - Images of the building

**Describe three questions that your site will answer:**
1. Where can I seek help if I'm homeless?
2. Where's the nearest place I can volunteer at?
3. Which cities have the most/least support?

Estimated Time to Completion:
40 hours

